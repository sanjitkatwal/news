<?php

namespace App\Http\Requests\Backend\UserMusic;

use Illuminate\Foundation\Http\FormRequest;

class UserMusicEditValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'filepath'          => 'mimes:mp3,wav,3gb,mpga',
            'cover_photo'       => 'mimes:jpeg,jpg,png,gif',
            'name'              => 'required',
            'genre'             => 'required',
            'length'            => 'required',
            'artists'           => 'required',
        ];
    }

    public function messages()
    {
        return [
            'filepath.required'             => 'The music file is required',
        ];
    }
}
