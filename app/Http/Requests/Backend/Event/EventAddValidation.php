<?php

namespace App\Http\Requests\Backend\Event;

use Illuminate\Foundation\Http\FormRequest;

class EventAddValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'event_title'              => 'required',
            'address_string'           => 'required',
            'street'                   => 'required',
            'suburb'                   => 'required',
            'postcode'                 => 'required',
            'state'                    => 'required',
            'country'                  => 'required',
            'event_type'               => 'required',
            'event_start_date'         => 'required',
            'event_end_date'           => 'required',
            'event_start_time'         => 'required',
            'description'              => 'required',
            'lat'                      => 'required',
            'lon'                      => 'required',
        ];
    }
}
