<?php

namespace App\Http\Requests\Backend\BusinessGallery;

use Illuminate\Foundation\Http\FormRequest;

class BusinessEditGalleryValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'business_id'       => 'required',
            'filepath'          => 'required',
            'media_type'        => 'required',
            'caption'           => 'required',
            'description'       => 'required',
        ];
    }
}
