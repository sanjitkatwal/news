<?php

namespace App\Http\Requests\Backend\BusinessOffer;

use Illuminate\Foundation\Http\FormRequest;

class BusinessAddValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'business_id'                   =>'required',
            'discount_percentage'           =>'required',
            'discount_amount'               =>'required',
            'description'                   =>'required',
            'type'                          =>'required',
            'expiry_date'                   =>'required',
            'terms'                         =>'required',
            'address_string'                =>'required',
            'street'                        =>'required',
            'suburb'                        =>'required',
            'postcode'                      =>'required',
            'state'                         =>'required',
            'country'                       =>'required',
            'lat'                           =>'required',
            'lon'                           =>'required',
        ];
    }
}
