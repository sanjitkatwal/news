<?php

namespace App\Http\Requests\Backend\BusinessPortfolios;

use Illuminate\Foundation\Http\FormRequest;

class PrtfoliosEditValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'business_id'       => 'required',
            'name'              => 'required',
            'description'       => 'required',
            'type'              => 'required',
            'date'              => 'required',
            'address_string'    => 'required',
            'street'              => 'required',
            'suburb'            => 'required',
            'postcode'          => 'required',
            'state'             => 'required',
            'country'           => 'required',
            'lat'               => 'required',
            'lon'               => 'required',
        ];
    }

    public function messages()
    {
        return [
            'business_id.required'          => 'Select at least one business name',
            'lat.required'                  => 'The latitude field is required.',
            'lon.required'                  => 'The longitude field is required.',
        ];
    }
}
