<?php

namespace App\Http\Requests\Backend\Business;

use Illuminate\Foundation\Http\FormRequest;

class BusinessAddValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'business_name'             => 'required',
            'business_type'             => 'required',
            'email'                     => 'required|email',
            'phone'                     => 'required|numeric',
            'total_employees'           => 'required|numeric',
            'opening_hour'              => 'required',
            'established'               => 'required',
            'max_customer_handle'       => 'required|numeric',
            'website_link'              => 'required',
            'feature'                   => 'required',
            'abn_acn'                   => 'required',
            'about_business'            => 'required',
            'street_address'            => 'required',
            'suburb'                    => 'required',
            'postcode'                  => 'required',
            'state'                     => 'required',
            'country'                   => 'required',
            'address_string'            => 'required',
            'lat'                       => 'required',
            'lon'                       => 'required',
            'operating_radius'          => 'required',
            'instagram'                 => 'required',
            'twitter'                   => 'required',
            'facebook'                  => 'required',
            'business_profile_image'    => 'image|mimes:jpeg,png,jpg,gif',
            'business_cover_image'      => 'image|mimes:jpeg,png,jpg,gif',
        ];
    }

    public function messages()
    {
        return [
            'phone.numeric'                => 'This must be in nuber only.',
            'total_employees.numeric'      => 'Employees must be in number only',
            'max_customer_handle.numeric'  => 'Customer handle must be in number only',
        ];
    }
}
