<?php

namespace App\Http\Requests\Backend\Music;

use Illuminate\Foundation\Http\FormRequest;

class MusicEditValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'business_id'       => 'required',
            'filepath'          => 'mimes:mp3,wav,3gb',
            'cover_photo'       => 'mimes:jpeg,jpg,png,gif',
            'name'              => 'required',
            'genre'             => 'required',
            'lengths'           => 'required',
            'artists'           => 'required',
        ];
    }

    public function messages()
    {
        return [
            'business_id.required'          => 'Select at least one business name',
        ];
    }
}
