<?php

namespace App\Http\Requests\Backend\Music;

use Illuminate\Foundation\Http\FormRequest;

class MusicAddValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'business_id'       => 'required',
            'filepath'          => 'mimes:mp3,wav,3gb,mpga|required',
            'cover_photo'       => 'mimes:jpeg,jpg,png,gif|required',
            'name'              => 'required',
            'genre'             => 'required',
            'length'            => 'required',
            'artists'           => 'required',
        ];
    }

    public function messages()
    {
        return [
            'filepath.required'             => 'The file is required',
            'business_id.required'          => 'Select at least one business name',
        ];
    }
}
