<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Music\MusicAddValidation;
use App\Http\Requests\Backend\Music\MusicEditValidation;
use App\Models\Business;
use App\Models\BusinessGallery;
use App\Models\BusinessMusic;
use Illuminate\Http\Request;
use File, Size;

class BusinessMusicController extends AdminBaseController
{
    protected $music_folder_path;
    protected $folder_path;

    protected $business_musics = 'business_musics';
    protected $business_musics_cover_image = 'music_cover_images';

    public function __construct(){
        $this->music_folder_path = public_path().DIRECTORY_SEPARATOR.'storage'.DIRECTORY_SEPARATOR.$this->business_musics.DIRECTORY_SEPARATOR;
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'storage'.DIRECTORY_SEPARATOR.$this->business_musics_cover_image.DIRECTORY_SEPARATOR;
    }

    public function index()
    {
        return view('backend.business_music.index')
            ->with('business_galleries', BusinessMusic::all())
            ->with('no', 1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['business']   = Business::select('id', 'business_name')->orderBy('business_name', 'asc')->get();

        return view('backend.business_music.create', compact('data'));
    }

    public function store(MusicAddValidation $request)
    {
        if ($request->hasFile('filepath') or $request->hasFile('NULL')){
            parent::checkBusinessMusicFolderExists();

            $file_size = $request->file('filepath')->getSize();

            $business_music_file = $request->file('filepath');
            $new_business_music_name = rand(4747, 9876).'_'.$business_music_file->getClientOriginalName();
            $business_music_file->move($this->music_folder_path, $new_business_music_name);
        }

        if ($request->hasFile('cover_photo')){
            parent::checkFolderExists();

            $business_cover_image = $request->file('cover_photo');
            $new_business_cover_image_name = rand(4747, 9876).'_'.$business_cover_image->getClientOriginalName();
            $business_cover_image->move($this->folder_path, $new_business_cover_image_name);
        }
        BusinessMusic::create([
            'name'                                => $request->get('name'),
            'length'                              => $request->get('length'),
            'artists'                             => $request->get('artists'),
            'genre'                               => $request->get('genre'),
            'business_id'                         => $request->get('business_id'),
            'filepath'                            => isset($new_business_music_name)?$new_business_music_name:'',
            'filesize'                            => $file_size,
            'cover_photo'                         => isset($new_business_cover_image_name)?$new_business_cover_image_name:'',
        ]);

        $request->session()->flash('success', 'Data added Successfully.');

        return redirect(route('admin.business-music.list'));
    }

    public function show($id)
    {
        //
    }

    public function edit(Request $request, $id)
    {
        $data = [];
        if(!$data['row'] = BusinessMusic::find($id)){
            return redirect()->route('admin.error', ['code'=>'500']);
        }
        $data['business']   = Business::select('id', 'business_name')
            ->orderBy('business_name', 'asc')
            ->get();

        return view('backend.business_music.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        if(!$business_music = BusinessMusic::find($id))
            return redirect()->route('admin.error', ['code'=>'500']);

        if ($request->hasFile('filepath') or $request->hasFile('NULL')){
            parent::checkBusinessMusicFolderExists();

            $file_size = $request->file('filepath')->getSize();

            $business_music_file = $request->file('filepath');
            $new_business_music_name = rand(4747, 9876).'_'.$business_music_file->getClientOriginalName();
            $business_music_file->move($this->music_folder_path, $new_business_music_name);
        }

        if ($request->hasFile('cover_photo')){
            parent::checkFolderExists();

            $business_cover_image = $request->file('cover_photo');
            $new_business_cover_image_name = rand(4747, 9876).'_'.$business_cover_image->getClientOriginalName();
            $business_cover_image->move($this->folder_path, $new_business_cover_image_name);
        }
        $business_music->update([
            'name'                                => $request->get('name'),
            'length'                              => $request->get('length'),
            'artists'                             => $request->get('artists'),
            'genre'                               => $request->get('genre'),
            'business_id'                         => $request->get('business_id'),
            'filepath'                            => isset($new_business_music_name)?$new_business_music_name:$business_music->filepath,
            'filesize'                            => isset($file_size)?$file_size:$business_music->filesize,
            'cover_photo'                         => isset($new_business_cover_image_name)?$new_business_cover_image_name:$business_music->cover_photo,
        ]);

        $request->session()->flash('success', 'Data Updated Successfully.');

        return redirect(route('admin.business-music.list'));
    }

    public function destroy(Request $request, $id)
    {
        if(!$business_music = BusinessMusic::find($id))
            return redirect()->route('admin.error', ['code'=>'500']);

        if($business_music->filepath){
            //remove old image
            if($this->music_folder_path.$business_music->filepath){
                File::delete($this->music_folder_path.$business_music->filepath);
            }
        }
        //check image if exist then firs delete image than row
        if($business_music->cover_photo){
            //remove old image
            if($this->folder_path.$business_music->cover_photo){
                File::delete($this->folder_path.$business_music->cover_photo);
            }
        }

        $business_music->delete();
        $request->session()->flash('success', 'Data Deleted Successfully.');

        return redirect(route('admin.business-music.list'));
    }
}
