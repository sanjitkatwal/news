<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\Http\Requests\Backend\Music\MusicAddValidation;
use App\Http\Requests\Backend\UserMusic\UserMusicAddValidation;
use App\Http\Requests\Backend\UserMusic\UserMusicEditValidation;
use App\Models\UserMusic;
use Illuminate\Http\Request;
use File, Size;
use Illuminate\Support\Facades\Auth;

class UserMusicController extends AdminBaseController
{
    protected $music_folder_path;
    protected $folder_path;
    protected $storage_path;

    protected $user_musics = 'user_musics';
    protected $user_musics_cover_image = 'user_music_cover_images';

    public function __construct(){

        $this->storage_path = public_path().DIRECTORY_SEPARATOR.'storage'.DIRECTORY_SEPARATOR;
        $this->music_folder_path = public_path().DIRECTORY_SEPARATOR.'storage'.DIRECTORY_SEPARATOR.$this->user_musics.DIRECTORY_SEPARATOR;
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'storage'.DIRECTORY_SEPARATOR.$this->user_musics_cover_image.DIRECTORY_SEPARATOR;
    }

    public function index()
    {
        return view('backend.user_music.index')
            ->with('user_musics', UserMusic::all())
            ->with('no', 1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.user_music.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserMusicAddValidation $request)
    {
        if ($request->hasFile('filepath') or $request->hasFile('NULL')){
            parent::checkBusinessMusicFolderExists();

            $file_size = $request->file('filepath')->getSize();

            $user_music_file     = $request->file('filepath');
            $new_user_music_name = $user_music_file->getClientOriginalName();
            $filepath            = $this->user_musics.'/'.$user_music_file->getClientOriginalName();
            $user_music_file->move($this->music_folder_path, $new_user_music_name);
        }

        if ($request->hasFile('cover_photo')){
            parent::checkFolderExists();

            $user_cover_image = $request->file('cover_photo');
            $new_user_cover_image_name = rand(4747, 9876).'_'.$user_cover_image->getClientOriginalName();
            $user_cover_image->move($this->folder_path, $new_user_cover_image_name);
        }
        UserMusic::create([
            'user_id'                             => Auth::user()->id,
            'name'                                => $request->get('name'),
            'length'                              => $request->get('length'),
            'artists'                             => $request->get('artists'),
            'genre'                               => $request->get('genre'),
            'filepath'                            => isset($filepath)?$filepath:'',
            'filesize'                            => $file_size,
            'cover_photo'                         => isset($new_user_cover_image_name)?$new_user_cover_image_name:'',
        ]);

        $request->session()->flash('success', 'Data added Successfully.');

        return redirect(route('admin.user-music.list'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data = [];
        if(!$data['row'] = UserMusic::find($id)){
            return redirect()->route('admin.error', ['code'=>'500']);
        }

        return view('backend.user_music.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserMusicEditValidation $request, $id)
    {
        if(!$user_music = UserMusic::find($id))
            return redirect()->route('admin.error', ['code'=>'500']);

        if ($request->hasFile('filepath') or $request->hasFile('NULL')){
            parent::checkBusinessMusicFolderExists();

            if($user_music->filepath && file_exists($this->storage_path.$user_music->filepath)){
                //remove old image
                File::delete($this->storage_path.$user_music->filepath);
            }

            $file_size = $request->file('filepath')->getSize();

            $user_music_file     = $request->file('filepath');
            $new_user_music_name = $user_music_file->getClientOriginalName();
            $filepath            = $this->user_musics.'/'.$user_music_file->getClientOriginalName();
            $user_music_file->move($this->music_folder_path, $new_user_music_name);
        }

        if ($request->hasFile('cover_photo')){
            parent::checkFolderExists();

            if($user_music->cover_photo && file_exists($this->folder_path.$user_music->cover_photo)){
                //remove old image
                File::delete($this->folder_path.$user_music->cover_photo);
            }

            $user_cover_image = $request->file('cover_photo');
            $new_user_cover_image_name = rand(4747, 9876).'_'.$user_cover_image->getClientOriginalName();
            $user_cover_image->move($this->folder_path, $new_user_cover_image_name);
        }

        $user_music->update([
            'name'                                => $request->get('name'),
            'length'                              => $request->get('length'),
            'artists'                             => $request->get('artists'),
            'genre'                               => $request->get('genre'),
            'business_id'                         => $request->get('business_id'),
            'filepath'                            => isset($filepath)?$filepath:$user_music->filepath,
            'filesize'                            => isset($file_size)?$file_size:$user_music->filepath,
            'cover_photo'                         => isset($new_user_cover_image_name)?$new_user_cover_image_name:$user_music->cover_photo,
        ]);

        $request->session()->flash('success', 'Data Updated Successfully.');

        return redirect(route('admin.user-music.list'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if(!$business_music = BusinessMusic::find($id))
            return redirect()->route('admin.error', ['code'=>'500']);

        if($business_music->filepath){
            //remove old image
            if($this->music_folder_path.$business_music->filepath){
                File::delete($this->music_folder_path.$business_music->filepath);
            }
        }
        //check image if exist then firs delete image than row
        if($business_music->cover_photo){
            //remove old image
            if($this->folder_path.$business_music->cover_photo){
                File::delete($this->folder_path.$business_music->cover_photo);
            }
        }

        $business_music->delete();
        $request->session()->flash('success', 'Data Deleted Successfully.');

        return redirect(route('admin.music.list'));
    }
}
