<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Models\UserManager;
use App\Models\UserProfile;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use File, Redirect, View;
use App\Mail\sendNotification;

class UserManagerController extends AdminBaseController
{
    protected $profile_image_path;
    protected $profile_cover_image_path;
    protected $default_music_path;

    protected $user_profile_image = 'user_profile_images';
    protected $user_cover_image = 'user_cover_images';
    protected $user_default_music = 'user_default_musics';

    public function __construct()
    {
        $this->profile_image_path = public_path() . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . $this->user_profile_image . DIRECTORY_SEPARATOR;
        $this->profile_cover_image_path = public_path() . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . $this->user_cover_image . DIRECTORY_SEPARATOR;
        $this->default_music_path = public_path() . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . $this->user_default_music . DIRECTORY_SEPARATOR;
    }

    public function index()
    {
        //return view('backend.usermanager.index');
        return view('backend.usermanager.index')
            //->where('deleted_at' == '')
            ->with('users', UserManager::all()->where('deleted_at', Null))
            ->with('no', 1);
    }

    public function userCreate()
    {
        return view('backend.usermanager.user_create');
    }

    public function userStore(Request $request)
    {
        $this->validate(
            $request,
            [
                'name'                 => 'required',
                'email'                => "required|email|unique:users,email",
                'password'             => 'required|confirmed|min:6',
            ]
        );

        $user = User::create([
            'name'      => $request->get('name'),
            'email'     => $request->get('email'),
            'phone'     => $request->get('phone'),
            'password'  => Hash::make($request->get('password')),
        ]);

        $thisUser = User::findOrFail($user->id);
        $this->sendEmail($thisUser);

        $request->session()->flash('success', 'User added Successfully.');

        return redirect(route('admin.user-manager.list'));
    }

    public function sendEmail($thisUser)
    {
        Mail::to($thisUser['email'])->send(new sendNotification($thisUser));
    }
    public function userEdit($id)
    {
        $data = [];
        if(!$data['row'] = User::find($id)){
            return redirect()->route('admin.error', ['code'=>'500']);
        }

        return view('backend.usermanager.user_edit', compact('data'));
    }

    public function userUpdate(Request $request, $id)
    {
        if(!$user = User::find($id))
            return redirect()->route('admin.error', ['code'=>'500']);

        $password = Hash::make($request->get('password'));
        $user->update([
            'name'      => $request->get('name'),
            'email'     => $request->get('email'),
            'phone'     => $request->get('phone'),
            'password'  => isset($password)?$password:$user->password,

        ]);

        $request->session()->flash('success', 'User Updated Successfully.');
        return redirect(route('admin.user-manager.list'));
    }
    public function deletedList()
    {
        return view('backend.usermanager.deleted_index')
            //->where('deleted_at' == '')
            ->with('users', UserManager::all()->where('deleted_at', '!==', Null))
            ->with('no', 1);
    }
    public function create()
    {
        return view('backend.usermanager.create');
    }

    public function userProfilecreate()
    {

        $data['users'] = User::select('id', 'name')
            ->orderBy('name', 'asc')->get();

        return view('backend.usermanager.profile_create', compact('data'));
    }

    public function userProfileStore(Request $request)
    {
        $user_id = $request->get('user_id');
         $exist_user_id = UserProfile::where('user_id', $user_id)->first();
            //->where($user_id == UserProfile::select('user_id'));
        if ($exist_user_id) {

            if ($request->hasFile('profile_image') or $request->hasFile('NULL')) {
                parent::checkProfileImageFolderExists();

                //$file_size = $request->file('filepath')->getSize();

                $user_profile_image = $request->file('profile_image');
                $new_user_profile_image_name = $this->user_profile_image.'/'.rand(4747, 9876) . '_' . $user_profile_image->getClientOriginalName();
                $user_profile_image->move($this->profile_image_path, $new_user_profile_image_name);
                //dd($new_user_profile_image_name);
            }

            if ($request->hasFile('cover_image')) {
                parent::checkCoverImageFolderExists();

                $user_cover_image_name = $request->file('cover_image');
                $new_user_cover_image_name = $this->user_cover_image.'/'.rand(4747, 9876) . '_' . $user_cover_image_name->getClientOriginalName();
                $user_cover_image_name->move($this->profile_cover_image_path, $new_user_cover_image_name);
            }

            if ($request->hasFile('default_music')) {
                parent::checkDefaultMusicFolderExists();

                $user_default_music_name = $request->file('default_music');
                $new_user_default_music_name = $this->user_default_music.'/'.$user_default_music_name->getClientOriginalName();
                $user_default_music_name->move($this->default_music_path, $new_user_default_music_name);
            }

            $exist_user_id->update([
                'user_id'           => $request->get('user_id'),
                'nickname'          => $request->get('nickname'),
                'gender'            => $request->get('gender'),
                'marital_status'    => $request->get('marital_status'),
                'dob'               => $request->get('dob'),
                'phone'             => $request->get('phone'),
                'profession'        => $request->get('profession'),
                'address_string'    => $request->get('address_string'),
                'street'            => $request->get('street'),
                'suburb'            => $request->get('suburb'),
                'postcode'          => $request->get('postcode'),
                'state'             => $request->get('state'),
                'country'           => $request->get('country'),
                'summary'           => $request->get('summary'),
                'instagram'         => $request->get('instagram'),
                'facebook'          => $request->get('facebook'),
                'twitter'           => $request->get('twitter'),
                'profile_image'     => isset($new_user_profile_image_name) ? $new_user_profile_image_name : $exist_user_id->profile_image,
                'cover_image'       => isset($new_user_cover_image_name) ? $new_user_cover_image_name : $exist_user_id->cover_image,
                'default_music'     => isset($new_user_default_music_name) ? $new_user_default_music_name : $exist_user_id->default_music,
                'lat'               => $request->get('lat'),
                'lon'               => $request->get('lon'),
            ]);

            $request->session()->flash('success', 'Data added Successfully.');

            return redirect(route('admin.user-manager.list'));
        }

        return 'no';


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function userDelete(Request $request, $id)
    {
        if(!$user_profile = User::find($id))
            return redirect()->route('admin.error', ['code'=>'500']);

        /*//check image if exist then firs delete image than row
        if($business_gallery->filepath){
            //remove old image
            if($business_gallery->filepath){
                //remove old image
                File::delete($this->folder_path.$business_gallery->filepath);
            }
        }*/
        $user_profile->update([
            'deleted_at' => Carbon::now(),
        ]);
        $request->session()->flash('success', 'Data Deleted Successfully.');

        return redirect(route('admin.user-manager.list'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function permanentDestroy(Request $request, $id)
    {
        if(!$user = User::find($id))
            return redirect()->route('admin.error', ['code'=>'500']);


        $user->delete();
        $request->session()->flash('success', 'Data Deleted Successfully.');

        return redirect(route('admin.user-manager.list'));
    }

    public function userRecover(Request $request, $id)
    {
        if(!$user_profile = User::find($id))
            return redirect()->route('admin.error', ['code'=>'500']);

        $user_profile->update([
            'deleted_at' => Null,
        ]);
        $request->session()->flash('success', 'The '.$user_profile->name .' User is Recovered successfully.');

        return redirect(route('admin.user-manager.list'));

    }

    public function changeStatusForm(Request $request, $id){
        $data = [];
        if(!$data['row'] = User::find($id)){
            return redirect()->route('admin.error', ['code'=>'500']);
        }
        return view('backend.usermanager.status_update', compact('data'));

        //return view('backend.business.edit', compact('data'));
    }

    public function updateStatus(Request $request, $id)
    {
        if(!$user = User::find($id))
            return redirect()->route('admin.error', ['code'=>'500']);

        $user->update([
            'status' => $request->get('status'),
        ]);
        $request->session()->flash('success', 'The '.$user->name .' User status is changed.');

        return redirect(route('admin.user-manager.list'));
    }
}
