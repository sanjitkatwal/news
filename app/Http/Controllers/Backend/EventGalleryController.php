<?php

namespace App\Http\Controllers\Backend;

use File;
use App\Models\Event;
use App\Models\EventGallery;
use Illuminate\Http\Request;

class EventGalleryController extends AdminBaseController
{
    protected $folder_path;

    protected $event_gallery_images = 'event_gallery_images';

    public function __construct(){
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'storage'.DIRECTORY_SEPARATOR.$this->event_gallery_images.DIRECTORY_SEPARATOR;
    }

    public function index()
    {
        $data = [];
        $data['rows'] = EventGallery::all()->sortByDesc('id');
        return view('backend.event_gallery.index', compact('data'))->with('no', 1);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['event']   = Event::select('id', 'event_title')->orderBy('id', 'asc')->get();

        return view('backend.event_gallery.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('filepath')){
            parent::checkFolderExists();

            $event_gallery_image = $request->file('filepath');
            $new_event_gallery_image_name = $this->event_gallery_images.'/'.str_replace(' ', '_', $event_gallery_image->getClientOriginalName());
            $event_gallery_image->move($this->folder_path, $new_event_gallery_image_name);
        }
        EventGallery::create([
            'event_id'                            => $request->get('event_id'),
            'media_type'                          => $request->get('media_type'),
            'filepath'                            => isset($new_event_gallery_image_name)?$new_event_gallery_image_name:'',
            'caption'                             => $request->get('caption'),
            'description'                         => $request->get('description'),
        ]);

        $request->session()->flash('success', 'Data added Successfully.');

        return redirect(route('admin.event-galleries.list'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function edit(Request $request, $id)
    {
        $data = [];
        if(!$data['row'] = EventGallery::find($id)){
            return redirect()->route('admin.error', ['code'=>'500']);
        }
        $data['event']   = Event::select('id', 'event_title')
            ->orderBy('event_title', 'desc')
            ->get();

        return view('backend.event_gallery.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        if(!$event_gallery = EventGallery::find($id))
            return redirect()->route('admin.error', ['code'=>'500']);

        if ($request->hasFile('filepath')){
            parent::checkFolderExists();

            if($event_gallery->filepath && file_exists($this->folder_path.$event_gallery->filepath)){
                //remove old image
                File::delete($this->folder_path.$event_gallery->filepath);
            }

            $event_gallery_image = $request->file('filepath');
            $new_event_gallery_image_name = $this->event_gallery_images.'/'.rand(4747, 9876).'_'.$event_gallery_image->getClientOriginalName();
            $event_gallery_image->move($this->folder_path, $new_event_gallery_image_name);
        }
        $event_gallery->update([
            'event_id'                            => $request->get('event_id'),
            'media_type'                          => $request->get('media_type'),
            'filepath'                            => isset($new_event_gallery_image_name)?$new_event_gallery_image_name:$event_gallery->filepath,
            'caption'                             => $request->get('caption'),
            'description'                         => $request->get('description'),
        ]);

        $request->session()->flash('success', 'Data Updated Successfully.');

        return redirect(route('admin.event-galleries.list'));
    }

    public function destroy(Request $request, $id)
    {
        if(!$event_gallery = EventGallery::find($id))
            return redirect()->route('admin.error', ['code'=>'500']);

        //check image if exist then firs delete image than row
        if($event_gallery->filepath && $this->folder_path.$event_gallery->filepath){
            //remove old image
            if($event_gallery->filepath){
                //remove old image
                File::delete($this->folder_path.$event_gallery->filepath);
            }
        }

        $event_gallery->delete();
        $request->session()->flash('success', 'Data Deleted Successfully.');

        return redirect(route('admin.event-galleries.list'));
    }
}
