<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Business\BusinessAddValidation;
use App\Http\Requests\Backend\Business\BusinessEditValidation;
use App\Models\Business;
use App\Models\Event;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BusinessController extends AdminBaseController
{
    protected $profile_image_folder_path;
    protected $profile_cover_image_folder_path;
    protected $business_default_music_folder_path;
    protected $business_profile_images = 'business_profile_images';
    protected $business_cover_images = 'business_cover_images';
    protected $business_default_music = 'business_default_music';


    public function __construct(){
        $this->profile_image_folder_path = public_path().DIRECTORY_SEPARATOR.'storage'.DIRECTORY_SEPARATOR.$this->business_profile_images.DIRECTORY_SEPARATOR;
        $this->profile_cover_image_folder_path = public_path().DIRECTORY_SEPARATOR.'storage'.DIRECTORY_SEPARATOR.$this->business_cover_images.DIRECTORY_SEPARATOR;
        $this->business_default_music_folder_path = public_path().DIRECTORY_SEPARATOR.'storage'.DIRECTORY_SEPARATOR.$this->business_default_music.DIRECTORY_SEPARATOR;

    }
    public function index()
    {
        return view('backend.business.index')
            ->with('datas', Business::all())
            ->with('no', 1);
    }

 
    public function create()
    {
        return view('backend.business.create');
    }


    public function store(BusinessAddValidation $request)
    {
        if ($request->hasFile('business_profile_image')){
            parent::checkFolderExistsForProfileImage();

            $business_profile_image = $request->file('business_profile_image');
            $new_profile_image_name = $this->business_profile_images.'/'.rand(4747, 9876).'_'.$business_profile_image->getClientOriginalName();
            $business_profile_image->move($this->profile_image_folder_path, $new_profile_image_name);
        }

        if ($request->hasFile('business_cover_image')){
            parent::checkFolderExistsForBusinessCoverImage();

            $business_profile_cover_image = $request->file('business_cover_image');
            $new_profile_cover_image_name = $this->business_cover_images.'/'.rand(4747, 9876).'_'.$business_profile_cover_image->getClientOriginalName();
            $business_profile_cover_image->move($this->profile_cover_image_folder_path, $new_profile_cover_image_name);
        }

        if ($request->hasFile('default_music')){
            parent::checkFolderExistsForBusinessDefaultMusic();

            $business_profile_default_music = $request->file('default_music');
            $new_profile_default_music_name = $this->business_default_music.'/'.$business_profile_default_music->getClientOriginalName();
            $business_profile_default_music->move($this->business_default_music_folder_path, $new_profile_default_music_name);
        }

        Business::create([
            'user_id'                             => Auth::user()->id,
            'business_name'                       => $request->get('business_name'),
            'business_type'                       => $request->get('business_type'),
            'email'                               => $request->get('email'),
            'phone'                               => $request->get('phone'),
            'total_employees'                     => $request->get('total_employees'),
            'opening_hour'                        => $request->get('opening_hour'),
            'established'                         => $request->get('established'),
            'max_customer_handle'                 => $request->get('max_customer_handle'),
            'website_link'                        => $request->get('website_link'),
            'feature'                             => $request->get('feature'),
            'abn_acn'                             => $request->get('abn_acn'),
            'about_business'                      => $request->get('about_business'),
            'street_address'                      => $request->get('street_address'),
            'suburb'                              => $request->get('suburb'),
            'postcode'                            => $request->get('postcode'),
            'state'                               => $request->get('state'),
            'country'                             => $request->get('country'),
            'address_string'                      => $request->get('address_string'),
            'lat'                                 => $request->get('lat'),
            'lon'                                 => $request->get('lon'),
            'operating_radius'                    => $request->get('operating_radius'),
            'instagram'                           => $request->get('instagram'),
            'twitter'                             => $request->get('twitter'),
            'facebook'                            => $request->get('facebook'),
            'business_profile_image'              => isset($new_profile_image_name)?$new_profile_image_name:'',
            'business_cover_image'                => isset($new_profile_cover_image_name) ?$new_profile_cover_image_name:'',
            'default_music'                       => isset($new_profile_default_music_name)?$new_profile_default_music_name:'',
        ]);

        $request->session()->flash('success', 'Data added Successfully.');

        return redirect(route('admin.business-manager.list'));
    }


    public function show($id)
    {
        $data = [];
        if(!$data['row'] = Business::find($id)){
            return redirect()->route('admin.error', ['code'=>'500']);
        }

        return view('backend.business.show', compact('data'));
    }

    public function edit($id)
    {
        $data = [];
        if(!$data['row'] = Business::find($id)){
            return redirect()->route('admin.error', ['code'=>'500']);
        }

        return view('backend.business.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        if(!$business = Business::find($id))
            return redirect()->route('admin.error', ['code'=>'500']);

        if ($request->hasFile('business_profile_image')){
            parent::checkFolderExistsForProfileImage();

            if($business->business_profile_image){
                //remove old image
                File::delete($this->profile_image_folder_path.$business->business_profile_image);
            }

            $business_profile_image = $request->file('business_profile_image');
            $new_profile_image_name = $this->business_profile_images.'/'.rand(4747, 9876).'_'.$business_profile_image->getClientOriginalName();
            $business_profile_image->move($this->profile_image_folder_path, $new_profile_image_name);
        }

        if ($request->hasFile('business_cover_image')){
            parent::checkFolderExistsForBusinessCoverImage();

            if($business->business_cover_image){
                //remove old image
                File::delete($this->profile_cover_image_folder_path.$business->business_cover_image);
            }

            $business_profile_cover_image = $request->file('business_cover_image');
            $new_profile_cover_image_name = $this->business_cover_images.'/'.rand(4747, 9876).'_'.$business_profile_cover_image->getClientOriginalName();
            $business_profile_cover_image->move($this->profile_cover_image_folder_path, $new_profile_cover_image_name);
        }

        if ($request->hasFile('default_music')){
            parent::checkFolderExistsForBusinessDefaultMusic();

            if($business->business_cover_image){
                //remove old image
                File::delete($this->business_default_music_folder_path.$business->business_cover_image);
            }

            $business_profile_default_music = $request->file('default_music');
            $new_profile_default_music_name = $this->business_default_music.'/'.$business_profile_default_music->getClientOriginalName();
            $business_profile_default_music->move($this->business_default_music_folder_path, $new_profile_default_music_name);
        }


        $business->update([
            'user_id'                             => Auth::user()->id,
            'business_name'                       => $request->get('business_name'),
            'business_type'                       => $request->get('business_type'),
            'email'                               => $request->get('email'),
            'phone'                               => $request->get('phone'),
            'total_employees'                     => $request->get('total_employees'),
            'opening_hour'                        => $request->get('opening_hour'),
            'established'                         => $request->get('established'),
            'max_customer_handle'                 => $request->get('max_customer_handle'),
            'website_link'                        => $request->get('website_link'),
            'feature'                             => $request->get('feature'),
            'abn_acn'                             => $request->get('abn_acn'),
            'about_business'                      => $request->get('about_business'),
            'street_address'                      => $request->get('street_address'),
            'suburb'                              => $request->get('suburb'),
            'postcode'                            => $request->get('postcode'),
            'state'                               => $request->get('state'),
            'country'                             => $request->get('country'),
            'address_string'                      => $request->get('address_string'),
            'lat'                                 => $request->get('lat'),
            'lon'                                 => $request->get('lon'),
            'operating_radius'                    => $request->get('operating_radius'),
            'instagram'                           => $request->get('instagram'),
            'twitter'                             => $request->get('twitter'),
            'facebook'                            => $request->get('facebook'),
            'business_profile_image'              => isset($new_profile_image_name)?$new_profile_image_name:$business->business_profile_image,
            'business_cover_image'                => isset($new_profile_cover_image_name)?$new_profile_cover_image_name:$business->business_cover_image,
            'default_music'                       => isset($new_profile_default_music_name)?$new_profile_default_music_name:$business->default_music,
        ]);

        $request->session()->flash('success', 'Data Updated Successfully.');

        return redirect(route('admin.business-manager.list'));
    }

    public function destroy(Request $request, $id)
    {
        if(!$business = Business::find($id))
            return redirect()->route('admin.error', ['code'=>'500']);

        //check image if exist then firs delete image than row
        if($business->business_profile_image){
            //remove old image
            if($this->profile_image_folder_path.$business->business_profile_image){
                File::delete($this->profile_image_folder_path.$business->business_profile_image);
            }
        }

        if($business->business_cover_image){
            //remove old image
            if($this->profile_cover_image_folder_path.$business->business_cover_image){
                File::delete($this->profile_cover_image_folder_path.$business->business_cover_image);
            }
        }

        if($business->default_music){
            //remove old image
            if($this->business_default_music_folder_path.$business->default_music){
                File::delete($this->business_default_music_folder_path.$business->default_music);
            }
        }

        $business->delete();
        $request->session()->flash('success', 'Data Deleted Successfully.');

        return redirect(route('admin.business-manager.list'));
    }
}
