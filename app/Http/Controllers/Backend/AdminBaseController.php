<?php

namespace App\Http\Controllers\Backend;

use File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminBaseController extends Controller
{
    protected function checkFolderExists(){
        if (!file::exists($this->folder_path)){
            //create folder
            File::makeDirectory($this->folder_path);
        }
    }


    protected function checkFolderExistsForProfileImage(){
        if (!file::exists($this->profile_image_folder_path)){
            //create folder
            File::makeDirectory($this->profile_image_folder_path);
        }
    }
    protected function checkFolderExistsForBusinessCoverImage(){
        if (!file::exists($this->profile_cover_image_folder_path)){
            //create folder
            File::makeDirectory($this->profile_cover_image_folder_path);
        }
    }
    protected function checkFolderExistsForBusinessDefaultMusic(){
        if (!file::exists($this->business_default_music_folder_path)){
            //create folder
            File::makeDirectory($this->business_default_music_folder_path);
        }
    }

    //business music controller
    protected function checkBusinessMusicFolderExists(){
        if (!file::exists($this->music_folder_path)){
            //create folder
            File::makeDirectory($this->music_folder_path);
        }
    }


    //User Profile controller
    protected function checkProfileImageFolderExists(){
        if (!file::exists($this->profile_image_path)){
            //create folder
            File::makeDirectory($this->profile_image_path);
        }
    }

    protected function checkCoverImageFolderExists(){
        if (!file::exists($this->profile_cover_image_path)){
            //create folder
            File::makeDirectory($this->profile_cover_image_path);
        }
    }

    protected function checkDefaultMusicFolderExists(){
        if (!file::exists($this->default_music_path)){
            //create folder
            File::makeDirectory($this->default_music_path);
        }
    }

}
