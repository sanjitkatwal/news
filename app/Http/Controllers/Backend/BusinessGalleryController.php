<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Business\BusinessEditValidation;
use App\Http\Requests\Backend\BusinessGallery\BusinessAddGalleryValidation;
use App\MOdels\Business;
use App\Models\BusinessGallery;
use App\Models\BusinessPortfolio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use File;

class BusinessGalleryController extends AdminBaseController
{
    protected $folder_path;

    protected $business_gallery_images = 'business_gallery_images';

    public function __construct(){
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'storage'.DIRECTORY_SEPARATOR.$this->business_gallery_images.DIRECTORY_SEPARATOR;
    }

    public function index()
    {
        return view('backend.business_gallery.index')
            ->with('business_galleries', BusinessGallery::all())
            ->with('no', 1);
    }

    public function create()
    {
        $data['business']   = Business::select('id', 'business_name')->orderBy('business_name', 'asc')->get();

        return view('backend.business_gallery.create', compact('data'));
    }

    public function store(BusinessAddGalleryValidation $request)
    {
        if ($request->hasFile('filepath')){
            parent::checkFolderExists();

            $business_gallery_image = $request->file('filepath');
            $new_business_gallery_image_name = rand(4747, 9876).'_'.$business_gallery_image->getClientOriginalName();
            $business_gallery_image->move($this->folder_path, $new_business_gallery_image_name);
        }
        BusinessGallery::create([
            'business_id'                         => $request->get('business_id'),
            'media_type'                          => $request->get('media_type'),
            'filepath'                            => isset($new_business_gallery_image_name)?$new_business_gallery_image_name:'',
            'caption'                             => $request->get('caption'),
            'description'                         => $request->get('description'),
        ]);

        $request->session()->flash('success', 'Data added Successfully.');

        return redirect(route('admin.business-gallery.list'));
    }

    public function show($id)
    {
        //
    }

    public function edit(Request $request, $id)
    {
        $data = [];
        if(!$data['row'] = BusinessGallery::find($id)){
            return redirect()->route('admin.error', ['code'=>'500']);
        }
        $data['business']   = Business::select('id', 'business_name')
                            ->orderBy('business_name', 'asc')
                            ->get();

        return view('backend.business_gallery.edit', compact('data'));
    }


    public function update(BusinessEditValidation $request, $id)
    {
        if(!$business_gallery = BusinessGallery::find($id))
            return redirect()->route('admin.error', ['code'=>'500']);

        if ($request->hasFile('filepath')){
            parent::checkFolderExists();

            if($business_gallery->filepath && file_exists($this->folder_path.$business_gallery->filepath)){
                //remove old image
                File::delete($this->folder_path.$business_gallery->filepath);
            }

            $business_gallery_image = $request->file('filepath');
            $new_business_gallery_image_name = rand(4747, 9876).'_'.$business_gallery_image->getClientOriginalName();
            $business_gallery_image->move($this->folder_path, $new_business_gallery_image_name);
        }
        $business_gallery->update([
            'business_id'                         => $request->get('business_id'),
            'media_type'                          => $request->get('media_type'),
            'filepath'                            => isset($new_business_gallery_image_name)?$new_business_gallery_image_name:$business_gallery->filepath,
            'caption'                             => $request->get('caption'),
            'description'                         => $request->get('description'),
        ]);

        $request->session()->flash('success', 'Data Updated Successfully.');

        return redirect(route('admin.business-gallery.list'));
    }


    public function destroy(Request $request, $id)
    {
        if(!$business_gallery = BusinessGallery::find($id))
            return redirect()->route('admin.error', ['code'=>'500']);

        //check image if exist then firs delete image than row
        if($business_gallery->filepath){
            //remove old image
            if($business_gallery->filepath){
                //remove old image
                File::delete($this->folder_path.$business_gallery->filepath);
            }
        }

        $business_gallery->delete();
        $request->session()->flash('success', 'Data Deleted Successfully.');

        return redirect(route('admin.business-gallery.list'));
    }
}
