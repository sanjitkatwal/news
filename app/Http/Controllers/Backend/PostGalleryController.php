<?php

namespace App\Http\Controllers\Backend;

use File;
use App\Models\Post;
use App\Models\PostGallery;
use Illuminate\Http\Request;

class PostGalleryController extends AdminBaseController
{
    protected $folder_path;

    protected $post_gallery_images = 'post_gallery_images';

    public function __construct(){
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'storage'.DIRECTORY_SEPARATOR.$this->post_gallery_images.DIRECTORY_SEPARATOR;
    }

    public function index()
    {
        $data = [];
        $data['rows'] = PostGallery::all()->sortByDesc('id');
        return view('backend.post_gallery.index', compact('data'))->with('no', 1);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['post']   = Post::select('id', 'message')->orderBy('id', 'asc')->get();

        return view('backend.post_gallery.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('filepath')){
            parent::checkFolderExists();

            $post_gallery_image = $request->file('filepath');
            $new_post_gallery_image_name = $this->post_gallery_images.'/'.rand(4747, 9876).'_'.$post_gallery_image->getClientOriginalName();
            $post_gallery_image->move($this->folder_path, $new_post_gallery_image_name);
        }
        PostGallery::create([
            'post_id'                             => $request->get('post_id'),
            'media_type'                          => $request->get('media_type'),
            'filepath'                            => isset($new_post_gallery_image_name)?$new_post_gallery_image_name:'',
            'caption'                             => $request->get('caption'),
            'description'                         => $request->get('description'),
        ]);

        $request->session()->flash('success', 'Data added Successfully.');

        return redirect(route('admin.post-galleries.list'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data = [];
        if(!$data['row'] = PostGallery::find($id)){
            return redirect()->route('admin.error', ['code'=>'500']);
        }

        $data['post']   = Post::select('id', 'message')->orderBy('id', 'asc')->get();
        return view('backend.post_gallery.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        if(!$post_gallery = PostGallery::find($id))
            return redirect()->route('admin.error', ['code'=>'500']);

        if ($request->hasFile('filepath')){
            parent::checkFolderExists();

            if($post_gallery->filepath && file_exists($this->folder_path.$post_gallery->filepath)){
                //remove old image
                File::delete($this->folder_path.$post_gallery->filepath);
            }

            $post_gallery_image = $request->file('filepath');
            $new_post_gallery_image_name = $this->post_gallery_images.'/'.rand(4747, 9876).'_'.$post_gallery_image->getClientOriginalName();
            $post_gallery_image->move($this->folder_path, $new_post_gallery_image_name);
        }
        $post_gallery->update([
            'post_id'                             => $request->get('post_id'),
            'media_type'                          => $request->get('media_type'),
            'filepath'                            => isset($new_post_gallery_image_name)?$new_post_gallery_image_name:$post_gallery->filepath,
            'caption'                             => $request->get('caption'),
            'description'                         => $request->get('description'),
        ]);

        $request->session()->flash('success', 'Data Updated Successfully.');

        return redirect(route('admin.post-galleries.list'));
    }

    public function destroy(Request $request, $id)
    {
        if(!$post_gallery = PostGallery::find($id))
            return redirect()->route('admin.error', ['code'=>'500']);

        //check image if exist then firs delete image than row
        if($post_gallery->filepath){
            //remove old image
            if($post_gallery->filepath){
                //remove old image
                File::delete($this->folder_path.$post_gallery->filepath);
            }
        }

        $post_gallery->delete();
        $request->session()->flash('success', 'Data Deleted Successfully.');

        return redirect(route('admin.post-galleries.list'));
    }
}
