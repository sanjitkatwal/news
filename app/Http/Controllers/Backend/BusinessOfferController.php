<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\BusinessOffer\BusinessEditValidation;
use App\Http\Requests\Backend\BusinessOffer\BusinessAddValidation;
use App\MOdels\Business;
use App\Models\BusinessOffer;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class BusinessOfferController extends Controller
{

    public function index()
    {
        return view('backend.business_offer.index')
            ->with('datas', BusinessOffer::all())
            ->with('no', 1);
    }

    public function create()
    {
        $data['business']   = Business::select('id', 'business_name')->orderBy('business_name', 'asc')->get();

        return view('backend.business_offer.create', compact('data'));

    }

    public function store(BusinessAddValidation $request)
    {
        /*if ($request->hasFile('brand_logo')){
            parent::checkFolderExists();

            $image          = $request->file('brand_logo');
            $new_image_name = rand(4747, 9876).'_'.$image->getClientOriginalName();
            $image->move($this->folder_path, $new_image_name);
        }*/

        BusinessOffer::create([
            'business_id'               => $request->get('business_id'),
            'discount_percentage'       => $request->get('discount_percentage'),
            'discount_amount'           => $request->get('discount_amount'),
            'description'               => $request->get('description'),
            'type'                      => $request->get('type'),
            'expiry_date'               => $request->get('expiry_date'),
            'terms'                     => $request->get('terms'),
            'address_string'            => $request->get('address_string'),
            'street'                    => $request->get('street'),
            'suburb'                    => $request->get('suburb'),
            'postcode'                  => $request->get('postcode'),
            'state'                     => $request->get('state'),
            'country'                   => $request->get('country'),
            'lat'                       => $request->get('lat'),
            'lon'                       => $request->get('lon'),
        ]);

        $request->session()->flash('success', 'Data added Successfully.');

        return redirect(route('admin.business-offer.list'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = [];
        $data['business']   = Business::select('id', 'business_name')->orderBy('business_name', 'desc')->get();

        if(!$data['row'] = BusinessOffer::find($id)){
            dd('No data found in that id');
            return redirect()->route('admin.error', ['code'=>'500']);
        }

        return view('backend.business_offer.edit', compact('data'));
    }

    public function update(BusinessEditValidation $request, $id)
    {
        if(!$businessOffer = BusinessOffer::find($id))
            return redirect()->route('admin.error', ['code'=>'500']);


        $businessOffer->update([
            'business_id'               => $request->get('business_id'),
            'discount_percentage'       => $request->get('discount_percentage'),
            'discount_amount'           => $request->get('discount_amount'),
            'description'               => $request->get('description'),
            'type'                      => $request->get('type'),
            'expiry_date'               => $request->get('expiry_date'),
            'terms'                     => $request->get('terms'),
            'address_string'            => $request->get('address_string'),
            'street'                    => $request->get('street'),
            'suburb'                    => $request->get('suburb'),
            'postcode'                  => $request->get('postcode'),
            'state'                     => $request->get('state'),
            'country'                   => $request->get('country'),
            'lat'                       => $request->get('lat'),
            'lon'                       => $request->get('lon'),
        ]);

        $request->session()->flash('success', 'Data Updated Successfully.');

        return redirect(route('admin.business-offer.list'));
    }

    public function destroy(Request $request, $id)
    {
        if(!$businessOffer = BusinessOffer::find($id))
            return redirect()->route('admin.error', ['code'=>'500']);

        $businessOffer->delete();
        $request->session()->flash('success', 'Data Deleted Successfully.');

        return redirect(route('admin.business-offer.list'));
    }
}
