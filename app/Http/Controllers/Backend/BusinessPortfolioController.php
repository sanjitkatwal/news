<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\BusinessPortfolios\PrtfoliosAddValidation;
use App\Http\Requests\Backend\BusinessPortfolios\PrtfoliosEditValidation;
use App\MOdels\Business;
use App\Models\BusinessPortfolio;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class BusinessPortfolioController extends Controller
{

    public function index()
    {
        return view('backend.business-portfolio.index')
            ->with('datas', BusinessPortfolio::all())
            ->with('no', 1);
    }

    public function create()
    {
        $data['business']   = Business::select('id', 'business_name')->orderBy('business_name', 'asc')->get();

        return view('backend.business-portfolio.create', compact('data'));

    }

    public function store(PrtfoliosAddValidation $request)
    {
        dd($request->all());
        BusinessPortfolio::create([
            'business_id'               => $request->get('business_id'),
            'name'                      => $request->get('name'),
            'description'               => $request->get('description'),
            'type'                      => $request->get('type'),
            'date'                      => $request->get('date'),
            'address_string'            => $request->get('address_string'),
            'street'                    => $request->get('street'),
            'suburb'                    => $request->get('suburb'),
            'postcode'                  => $request->get('postcode'),
            'state'                     => $request->get('state'),
            'country'                   => $request->get('country'),
            'lat'                       => $request->get('lat'),
            'lon'                       => $request->get('lon'),
        ]);

        $request->session()->flash('success', 'Data added Successfully.');

        return redirect(route('admin.business-portfolio.list'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = [];
        $data['business']   = Business::select('id', 'business_name')->orderBy('business_name', 'desc')->get();

        if(!$data['row'] = BusinessPortfolio::find($id)){
            dd('No data found in that id');
            return redirect()->route('admin.error', ['code'=>'500']);
        }

        return view('backend.business-portfolio.edit', compact('data'));
    }

    public function update(PrtfoliosEditValidation $request, $id)
    {
        if(!$portfolio = BusinessPortfolio::find($id))
            return redirect()->route('admin.error', ['code'=>'500']);


        $portfolio->update([
            'business_id'               => $request->get('business_id'),
            'name'                      => $request->get('name'),
            'description'               => $request->get('description'),
            'type'                      => $request->get('type'),
            'date'                      => $request->get('date'),
            'address_string'            => $request->get('address_string'),
            'street'                    => $request->get('street'),
            'suburb'                    => $request->get('suburb'),
            'postcode'                  => $request->get('postcode'),
            'state'                     => $request->get('state'),
            'country'                   => $request->get('country'),
            'lat'                       => $request->get('lat'),
            'lon'                       => $request->get('lon'),
        ]);

        $request->session()->flash('success', 'Data Updated Successfully.');

        return redirect(route('admin.business-portfolio.list'));
    }

    public function destroy(Request $request, $id)
    {
        if(!$portfolio = BusinessPortfolio::find($id))
            return redirect()->route('admin.error', ['code'=>'500']);

        $portfolio->delete();
        $request->session()->flash('success', 'Data Deleted Successfully.');

        return redirect(route('admin.business-portfolio.list'));
    }
}
