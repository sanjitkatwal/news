<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Event\EventAddValidation;
use App\Http\Requests\Backend\Event\EventEditValidation;
use App\Models\BusinessPortfolio;
use App\Models\Event;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = [];
        $data['rows'] = Event::all()->sortByDesc('id');
        return view('backend.event.index', compact('data'))->with('no', 1);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.event.create');
    }

    public function store(Request $request)
    {
        Event::create([
            'creator_id'                => Auth::user()->id,
            'event_title'               => $request->get('event_title'),
            'address_string'            => $request->get('address_string'),
            'street'                    => $request->get('street'),
            'suburb'                    => $request->get('suburb'),
            'postcode'                  => $request->get('postcode'),
            'state'                     => $request->get('state'),
            'country'                   => $request->get('country'),
            'event_type'                => $request->get('event_type'),
            'event_start_date'          => $request->get('event_start_date'),
            'event_end_date'            => $request->get('event_end_date'),
            'event_start_time'          => $request->get('event_start_time'),
            'description'               => $request->get('description'),
            'lat'                       => $request->get('lat'),
            'lon'                       => $request->get('lon'),
        ]);

        $request->session()->flash('success', 'Data added Successfully.');

        return redirect(route('admin.event-manager.list'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        if(!$data['row'] = Event::find($id)){
            return redirect()->route('admin.error', ['code'=>'500']);
        }

        return view('backend.event.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if(!$event = Event::find($id))
            return redirect()->route('admin.error', ['code'=>'500']);
            //return redirect()->route('admin.error', ['code'=>'500']);

        $event->update([
            'creator_id'                => Auth::user()->id,
            'event_title'               => $request->get('event_title'),
            'address_string'            => $request->get('address_string'),
            'street'                    => $request->get('street'),
            'suburb'                    => $request->get('suburb'),
            'postcode'                  => $request->get('postcode'),
            'state'                     => $request->get('state'),
            'country'                   => $request->get('country'),
            'event_type'                => $request->get('event_type'),
            'event_start_date'          => $request->get('event_start_date'),
            'event_end_date'            => $request->get('event_end_date'),
            'event_start_time'          => $request->get('event_start_time'),
            'description'               => $request->get('description'),
            'lat'                       => $request->get('lat'),
            'lon'                       => $request->get('lon'),
        ]);

        $request->session()->flash('success', 'Data Updated Successfully.');

        return redirect(route('admin.event-manager.list'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if(!$event = Event::find($id))
            return redirect()->route('admin.error', ['code'=>'500']);

        $event->delete();
        $request->session()->flash('success', 'Data Deleted Successfully.');

        return redirect(route('admin.event-manager.list'));
    }
}
