<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessGallery extends Model
{
    public $table = "business_galleries";
    protected $fillable = [
        'id', 'business_id','filepath', 'media_type', 'caption', 'description',
        'created_at', 'updated_at'
    ];


    public function business()
    {
        return $this->belongsTo(Business::class);
    }
}
