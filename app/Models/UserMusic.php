<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserMusic extends Model
{
    public $table = "user_musics";
    protected $fillable = [
        'id', 'user_id','filepath', 'filesize', 'cover_photo', 'name', 'length',
        'artists', 'genre','created_at', 'updated_at'
    ];



    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
