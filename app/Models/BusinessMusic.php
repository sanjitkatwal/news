<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessMusic extends Model
{
    public $table = "business_musics";
    protected $fillable = [
        'id', 'business_id','filepath', 'filesize', 'cover_photo', 'name', 'length',
        'artist', 'genre','created_at', 'updated_at'
    ];



    public function business()
    {
        return $this->belongsTo(Business::class);
    }
}
