<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserManager extends Model
{
    public $table = "users";
    protected $fillable = [
        'id', 'name', 'email', 'phone', 'email_verified_at', 'phone_verified_at',
        'password', 'remember_token', 'created_at', 'updated_at', 'deleted_at', 'gcm_token'
    ];
}
