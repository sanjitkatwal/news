<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventGallery extends Model
{
    public $table = "event_galleries";
    protected $fillable = [
        'event_id', 'filepath','media_type', 'caption', 'description',
        'created_at', 'updated_at'
    ];

    public function event()
    {
        return $this->belongsTo(Event::class);
    }
}
