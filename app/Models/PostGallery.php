<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostGallery extends Model
{
    public $table = "post_galleries";
    protected $fillable = [
        'post_id', 'filepath','media_type', 'caption', 'description',
        'created_at', 'updated_at'
    ];
}
