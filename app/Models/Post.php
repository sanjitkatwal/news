<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public $table = "posts";
    protected $fillable = [
        'id','user_id','message','visibility_type', 'event_id','business_id',
        'posting_as_id','posting_as_type','shared_id','business_offer_id','business_portfolio_id',
        'live_key','source_country','created_at','updated_at'
    ];
}
