<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    public $table = "reports";
    protected $fillable = [
        'message', 'file_attachment','reportable_type', 'reportable_id', 'reporter_id',
        'created_at', 'updated_at'
    ];
}
