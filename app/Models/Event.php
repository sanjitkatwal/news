<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public $table = "events";
    protected $fillable = [
        'event_title', 'creator_id','address_string', 'street', 'suburb', 'postcode',
        'state', 'country', 'event_type', 'event_start_date', 'event_end_date',
        'event_start_time', 'description', 'created_at', 'updated_at', 'lat', 'lon'
    ];

    public function eventGalleries()
    {
        return $this->hasMany(EventGallery::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
