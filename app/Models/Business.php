<?php

namespace App\MOdels;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    public $table = "businesses";
    protected $fillable = [
        'id','user_id', 'business_name','business_type', 'email', 'phone', 'total_employees', 'opening_hour',
        'established', 'max_customer_handle', 'website_link', 'feature', 'abn_acn', 'about_business',
        'street_address', 'suburb', 'postcode', 'state', 'country', 'address_string', 'lat', 'lon', 'operating_radius',
        'instagram','twitter','facebook','business_profile_image','business_cover_image', 'default_music'
    ];


    public function businessGalleries()
    {
        return $this->hasMany(BusinessGallery::class);
    }


    public function businessMusics()
    {
        return $this->hasMany(BusinessMusic::class);
    }

    public function businessPortfolios()
    {
        return $this->hasMany(BusinessPortfolio::class);
    }
    public function businessOffers()
    {
        return $this->hasMany(BusinessOffer::class);
    }
}
