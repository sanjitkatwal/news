<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    public $table = "user_profiles";
    protected $fillable = [
        'id', 'user_id','nickname', 'gender', 'marital_status', 'dob', 'phone',
        'profession', 'address_string','street','suburb','postcode','state',
        'country','summary','instagram','facebook','twitter','profile_image',
        'cover_image','default_music','lat','lon','deleted_at','created_at', 'updated_at'
    ];


}
