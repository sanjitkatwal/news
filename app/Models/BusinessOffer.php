<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessOffer extends Model
{
    public $table = "business_offers";
    protected $fillable = [
        'business_id', 'discount_percentage', 'discount_amount', 'description', 'type',
        'expiry_date', 'terms', 'address_string', 'street', 'suburb', 'postcode','state',
        'country', 'lat', 'lon', 'created_at', 'updated_at'
    ];


    public function business()
    {
        return $this->belongsTo(Business::class);
    }


}
