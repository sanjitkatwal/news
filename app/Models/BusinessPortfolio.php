<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessPortfolio extends Model
{
    public $table = "business_portfolios";
    protected $fillable = [
        'business_id', 'name','description', 'type', 'date', 'address_string', 'street',
        'suburb', 'postcode','state', 'country', 'lat', 'lon', 'created_at', 'updated_at'
    ];


    public function business()
    {
        return $this->belongsTo(Business::class);
    }


}
