@extends('backend.layouts.app')
@section('page_title')
    CMS-User-Music-Manager
@endsection

@section('breadcomes')
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashbaord</a></li>
        <li class="breadcrumb-item active">User Music Create</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">User Music Create <small>header small text goes here...</small></h1>
    <!-- end page-header -->
@endsection

@section('content')
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
            <!-- begin panel-heading -->
            <div class="panel-heading ui-sortable-handle">
                <h4 class="panel-title">Form Controls</h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <!-- end panel-heading -->
            <!-- begin panel-body -->
            <div class="panel-body">
                <form action="{{ route('admin.user-music.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Music Name</label>
                        <div class="col-md-9">
                            <input type="text" name="name" value="{{ old('name')?old('name'):'' }}" class="form-control" placeholder="Enter Music Name ....." />
                            <medium class="text-danger" >{{ $errors->first('name') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Music Lenght</label>
                        <div class="col-md-9">
                            <input type="text" name="length" value="{{ old('length')?old('length'):'' }}" class="form-control" placeholder="Enter Music Length ....." />
                            <medium class="text-danger" >{{ $errors->first('length') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Artist Name</label>
                        <div class="col-md-9">
                            <input type="text" name="artists" value="{{ old('artists')?old('artists'):'' }}" class="form-control" placeholder="Enter Artists Name ....." />
                            <medium class="text-danger" >{{ $errors->first('artists') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Genre</label>
                        <div class="col-md-9">
                            <input type="text" name="genre" value="{{ old('genre')?old('genre'):'' }}" class="form-control" placeholder="Enter Music Genre ....." />
                            <medium class="text-danger" >{{ $errors->first('genre') }}</medium>
                        </div>
                    </div>


                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Select Cover Image</label>
                        <div class="col-md-9">
                            <input type="file" name="cover_photo" {{ old('cover_photo')?old('cover_photo'):'' }} class="form-control"/>
                            <medium class="text-danger" >{{ $errors->first('cover_photo') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Select Music File</label>
                        <div class="col-md-9">
                            <input type="file" name="filepath" value="{{ old('filepath')?old('filepath'):'' }}" class="form-control"/>
                            <medium class="text-danger" >{{ $errors->first('filepath') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-0">
                        <label class="col-md-4 col-sm-4 col-form-label">&nbsp;</label>
                        <div class="col-md-8 col-sm-8">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>


                </form>
            </div>
            <!-- end panel-body -->

        </div>
        <!-- end panel -->

@endsection

