@extends('backend.layouts.app')
@section('page_title')
    CMS-Business-Offer-Manager
@endsection

@section('breadcomes')
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashbaord</a></li>
        <li class="breadcrumb-item active">Business Offer Edit</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Business Offer Edit <small>header small text goes here...</small></h1>
    <!-- end page-header -->
@endsection

@section('content')
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
            <!-- begin panel-heading -->
            <div class="panel-heading ui-sortable-handle">
                <h4 class="panel-title">Edit Control Form</h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <!-- end panel-heading -->
            <!-- begin panel-body -->
            <div class="panel-body">
                <form action="{{ route('admin.business-offer.update', $data['row']->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="portfolio_id" value="{{ $data['row']->id }}">
                    <div class="form-group row m-b-10">
                        <label class="col-form-label col-md-3">Business Name</label>
                        <div class="col-md-9">
                            <select name="business_id" class="form-control mb-3">
                                @if(count($data['business']) > 0)
                                    <option value="" select>Select Your Business Name</option>
                                    @foreach($data['business'] as $business)
                                        <option value="{{ $business->id }}" {{isset($data['row']) && $data['row']->business_id == $business->id?'selected':''}}>
                                            {!! $business->business_name !!}
                                        </option>
                                    @endforeach

                                @else
                                    <option>No Business Name Is Available.</option>
                                @endif
                            </select>
                            <medium class="text-danger" >{{ $errors->first('business_id') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Discount Persent (%)</label>
                        <div class="col-md-9">
                            <input type="text" name="discount_percentage" value="{{ isset($data['row']->discount_percentage)?$data['row']->discount_percentage:old('discount_percentage')  }}" class="form-control" placeholder="Discount Persent ....." />
                            <medium class="text-danger" >{{ $errors->first('discount_percentage') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Discount Amount</label>
                        <div class="col-md-9">
                            <input type="text" name="discount_amount" value="{{ isset($data['row']->discount_amount)?$data['row']->discount_amount:old('discount_amount') }}" class="form-control" placeholder="Enter Discount Amount ....." />
                            <medium class="text-danger" >{{ $errors->first('discount_amount') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Description</label>
                        <div class="col-md-9">
                            <textarea class="form-control ckeditor" id="editor1" name="description" rows="20">{{ isset($data['row']->description)?$data['row']->description:old('description') }}</textarea>
                        </div>
                        <medium class="text-danger" >{{ $errors->first('description') }}</medium>

                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Type</label>
                        <div class="col-md-9">
                            <input type="text" name="type" value="{{ isset($data['row']->type)?$data['row']->type:old('type')  }}" class="form-control" placeholder="Enter Type ....." />
                            <medium class="text-danger" >{{ $errors->first('type') }}</medium>
                        </div>

                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Expiry Date</label>
                        <div class="col-md-9">
                            <input type="text" id="datepicker" name="expiry_date" value="{{ isset($data['row']->expiry_date)?$data['row']->expiry_date:old('expiry_date') }}" class="form-control" placeholder="Enter Expiry Date in (MM/DD/YYYY) format ....." />
                            <medium class="text-danger" >{{ $errors->first('expiry_date') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Terms</label>
                        <div class="col-md-9">
                            <input type="text" name="terms" value="{{ isset($data['row']->terms)?$data['row']->terms:old('terms') }}" class="form-control" placeholder="Enter Terms ....." />
                            <medium class="text-danger" >{{ $errors->first('terms') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Address</label>
                        <div class="col-md-9">
                            <input type="text" name="address_string" value="{{$data['row']->address_string, old('address_string') }}" class="form-control" placeholder="Enter Address ....." />
                            <medium class="text-danger" >{{ $errors->first('address_string') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Street Name</label>
                        <div class="col-md-9">
                            <input type="text" name="street" value="{{ isset($data['row']->street)?$data['row']->street:old('street')  }}" class="form-control" placeholder="Enter Street Name ....." />
                            <medium class="text-danger" >{{ $errors->first('street') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Sub Urban</label>
                        <div class="col-md-9">
                            <input type="text" name="suburb" value="{{ isset($data['row']->suburb)?$data['row']->suburb:old('suburb') }}" class="form-control" placeholder="Enter Sub Urban....." />
                            <medium class="text-danger" >{{ $errors->first('suburb') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Post Code</label>
                        <div class="col-md-9">
                            <input type="text" name="postcode" value="{{ isset($data['row']->postcode)?$data['row']->postcode:old('postcode') }}" class="form-control" placeholder="Enter Post Code ....." />
                            <medium class="text-danger" >{{ $errors->first('postcode') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">State</label>
                        <div class="col-md-9">
                            <input type="text" name="state" value="{{ isset($data['row']->state)?$data['row']->state:old('state')  }}" class="form-control" placeholder="Enter State....." />
                            <medium class="text-danger" >{{ $errors->first('state') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Country</label>
                        <div class="col-md-9">
                            <input type="text" name="country" value="{{ isset($data['row']->country)?$data['row']->country:old('country')  }}" class="form-control" placeholder="Enter Contry....." />
                            <medium class="text-danger" >{{ $errors->first('country') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Latitude</label>
                        <div class="col-md-9">
                            <input type="text" name="lat" value="{{ isset($data['row']->lat)?$data['row']->lat:old('lat')  }}" class="form-control" placeholder="Enter String Latitude....." />
                            <medium class="text-danger" >{{ $errors->first('lat') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Longitude</label>
                        <div class="col-md-9">
                            <input type="text" name="lon" value="{{ isset($data['row']->lon)?$data['row']->lon:old('lon') }}" class="form-control" placeholder="Enter String Longitude....." />
                            <medium class="text-danger" >{{ $errors->first('lon') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-0">
                        <label class="col-md-4 col-sm-4 col-form-label">&nbsp;</label>
                        <div class="col-md-8 col-sm-8">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>


                </form>
            </div>
            <!-- end panel-body -->

        </div>
        <!-- end panel -->

@endsection

@section('page_specific_scripts')
    <script src="{{ asset('assets/admin/plugins/ckeditor/ckeditor.js') }}"></script>
    <script>
        $( "#datepicker" ).datepicker({
            format: "mm/dd/yy",
            weekStart: 0,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            rtl: true,
            orientation: "auto"
        });
    </script>
@endsection
