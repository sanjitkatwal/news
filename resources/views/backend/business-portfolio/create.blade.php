@extends('backend.layouts.app')
@section('page_title')
    CMS-Business-Manager
@endsection

@section('breadcomes')
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashbaord</a></li>
        <li class="breadcrumb-item active">Business Portfolio Create</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Business Portfolio Create <small>header small text goes here...</small></h1>
    <!-- end page-header -->
@endsection

@section('content')
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
            <!-- begin panel-heading -->
            <div class="panel-heading ui-sortable-handle">
                <h4 class="panel-title">Form Controls</h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <!-- end panel-heading -->
            <!-- begin panel-body -->
            <div class="panel-body">
                <form action="{{ route('admin.business-portfolio.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row m-b-10">
                            <label class="col-form-label col-md-3">Business Name</label>
                        <div class="col-md-9">
                            <select name="business_id" class="form-control mb-3">
                                @if(count($data['business']) > 0)
                                    <option value="" select>Select Your Business Name</option>
                                    @foreach($data['business'] as $business)
                                        <option value="{{ $business->id }}">{!! $business->business_name !!}</option>
                                    @endforeach

                                @else
                                    <option>No Business Name Is Available.</option>
                                @endif
                            </select>
                            <medium class="text-danger" >{{ $errors->first('business_id') }}</medium>
                        </div>
                    </div>
                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Name</label>
                        <div class="col-md-9">
                            <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Enter Name ....." />
                            <medium class="text-danger" >{{ $errors->first('name') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Description</label>
                        <div class="col-md-9">
                            <textarea class="form-control ckeditor" id="editor1" name="description" rows="20">{{ old('description') }}</textarea>
                            <medium class="text-danger">{{ $errors->first('description') }}</medium>
                        </div>
                    </div>


                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Type</label>
                        <div class="col-md-9">
                            <input type="text" name="business_type" value="{{ old('business_type') }}" class="form-control" placeholder="Enter Type ....." />
                            <medium class="text-danger" >{{ $errors->first('business_type') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Date</label>
                        <div class="col-md-9">
                            <input type="text" id="datepicker" name="date" value="{{ old('date') }}" class="form-control" placeholder="Enter Date in (MM/DD/YYYY) format ....." />
                            <medium class="text-danger" >{{ $errors->first('date') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Address</label>
                        <div class="col-md-9">
                            <input type="text" name="address_string" value="{{ old('address_string') }}" class="form-control" placeholder="Enter Address ....." />
                            <medium class="text-danger" >{{ $errors->first('address_string') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Street Name</label>
                        <div class="col-md-9">
                            <input type="text" name="street" value="{{ old('street') }}" class="form-control" placeholder="Enter Street Name ....." />
                            <medium class="text-danger" >{{ $errors->first('street') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Sub Urban</label>
                        <div class="col-md-9">
                            <input type="text" name="suburb" value="{{ old('suburb') }}" class="form-control" placeholder="Enter Sub Urban....." />
                            <medium class="text-danger" >{{ $errors->first('suburb') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Post Code</label>
                        <div class="col-md-9">
                            <input type="text" name="postcode" value="{{ old('postcode') }}" class="form-control" placeholder="Enter Post Code ....." />
                            <medium class="text-danger" >{{ $errors->first('postcode') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">State</label>
                        <div class="col-md-9">
                            <input type="text" name="state" value="{{ old('state') }}" class="form-control" placeholder="Enter State....." />
                            <medium class="text-danger" >{{ $errors->first('state') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Country</label>
                        <div class="col-md-9">
                            <input type="text" name="country" value="{{ old('country') }}" class="form-control" placeholder="Enter Contry....." />
                            <medium class="text-danger" >{{ $errors->first('country') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Latitude</label>
                        <div class="col-md-9">
                            <input type="text" name="lat" value="{{ old('lat') }}" class="form-control" placeholder="Enter String Latitude....." />
                            <medium class="text-danger" >{{ $errors->first('lat') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Longitude</label>
                        <div class="col-md-9">
                            <input type="text" name="lon" value="{{ old('lon') }}" class="form-control" placeholder="Enter String Longitude....." />
                            <medium class="text-danger" >{{ $errors->first('lon') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-0">
                        <label class="col-md-4 col-sm-4 col-form-label">&nbsp;</label>
                        <div class="col-md-8 col-sm-8">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>


                </form>
            </div>
            <!-- end panel-body -->

        </div>
        <!-- end panel -->

@endsection

@section('page_specific_scripts')
    <script src="{{ asset('assets/admin/plugins/ckeditor/ckeditor.js') }}"></script>
    <script>
        $( "#datepicker" ).datepicker({
            format: "mm/dd/yy",
            weekStart: 0,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            rtl: true,
            orientation: "auto"
        });
    </script>
@endsection
