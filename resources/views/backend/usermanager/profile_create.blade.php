@extends('backend.layouts.app')
@section('page_title')
    CMS-User-Profile-Manager
@endsection

@section('breadcomes')
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashbaord</a></li>
        <li class="breadcrumb-item active">User Profile Create</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">User Profile Create <small>header small text goes here...</small></h1>
    <!-- end page-header -->
@endsection

@section('content')

            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
            <!-- begin panel-heading -->
            <div class="panel-heading ui-sortable-handle">
                <h4 class="panel-title">Form Controls</h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <!-- end panel-heading -->
            <!-- begin panel-body -->
            <div class="panel-body">
                <form action="{{ route('admin.user-profile.store') }}" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">User Name</label>
                        <div class="col-md-9">
                            <select name="user_id" class="form-control mb-3">
                                @if(count($data['users']) > 0)
                                    <option value="" select>Select User Name</option>
                                    @foreach($data['users'] as $user)
                                        <option value="{{ $user->id }}">{!! $user->name !!}</option>
                                    @endforeach

                                @else
                                    <option>No User Name Is Available.</option>
                                @endif
                            </select>
                            <medium class="text-danger" >{{ $errors->first('user_id') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Nick Name</label>
                        <div class="col-md-9">
                            <input type="text" name="nickname" required  value="{{ old('nickname') }}" class="form-control" placeholder="Enter Nick Name ....." />
                            <medium class="text-danger" >{{ $errors->first('nickname') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Gender</label>
                        <div class="col-md-9">
                            <select name="gender" class="form-control mb-3" required="required">
                                    <option value="" select>Select Gender Name</option>
                                        <option value="0">Male</option>
                                        <option value="1">Female</option>
                                        <option value="2">Otheres</option>

                            </select>
                            <medium class="text-danger" >{{ $errors->first('gender') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Marital Status</label>
                        <div class="col-md-9">
                            <select name="marital_status" required="required" class="form-control mb-3">
                                <option value="" select>Select Marital Status</option>
                                <option value="0">Merried</option>
                                <option value="1">Unmerried</option>
                            </select>
                            <medium class="text-danger" >{{ $errors->first('marital_status') }}</medium>
                        </div>
                    </div>
                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Date Of Birth</label>
                        <div class="col-form-group m-b-9">
                            <input type="date" name="dob" required value="{{ old('dob') }}" class="form-control" />
                        </div>
                        <medium class="text-danger" >{{ $errors->first('marital_status') }}</medium>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Phone</label>
                        <div class="col-md-9">
                            <input type="text" name="phone" value="{{ old('phone') }}" class="form-control" placeholder="Enter Phone Number ....." />
                            <medium class="text-danger" >{{ $errors->first('phone') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Profession</label>
                        <div class="col-md-9">
                            <input type="text" name="profession" value="{{ old('profession') }}" class="form-control" placeholder="Enter Profession ....." />
                            <medium class="text-danger" >{{ $errors->first('profession') }}</medium>
                        </div>
                    </div>
                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Address</label>
                        <div class="col-md-9">
                            <input type="text" name="address_string" value="{{ old('address_string') }}" class="form-control" placeholder="Enter Address ....." />
                            <medium class="text-danger" >{{ $errors->first('address_string') }}</medium>
                        </div>
                    </div>
                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Street</label>
                        <div class="col-md-9">
                            <input type="text" name="street" value="{{ old('street') }}" class="form-control" placeholder="Enter Street ....." />
                            <medium class="text-danger" >{{ $errors->first('street') }}</medium>
                        </div>
                    </div>
                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Suburb</label>
                        <div class="col-md-9">
                            <input type="text" name="suburb" value="{{ old('suburb') }}" class="form-control" placeholder="Enter Suburb ....." />
                            <medium class="text-danger" >{{ $errors->first('suburb') }}</medium>
                        </div>
                    </div>
                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Post Code</label>
                        <div class="col-md-9">
                            <input type="text" name="postcode" value="{{ old('postcode') }}" class="form-control" placeholder="Enter Post Code ....." />
                            <medium class="text-danger" >{{ $errors->first('postcode') }}</medium>
                        </div>
                    </div>
                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">State</label>
                        <div class="col-md-9">
                            <input type="text" name="state" value="{{ old('state') }}" class="form-control" placeholder="Enter State ....." />
                            <medium class="text-danger" >{{ $errors->first('state') }}</medium>
                        </div>
                    </div>
                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Country</label>
                        <div class="col-md-9">
                            <input type="text" name="country" value="{{ old('country') }}" class="form-control" placeholder="Enter Country ....." />
                            <medium class="text-danger" >{{ $errors->first('country') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-lg-3 text-lg-right col-form-label">Description</label>
                        <div class="col-md-9">
                            <textarea class="form-control ckeditor" id="editor1" name="summary">
                                {{ old('summary') }}
                            </textarea>
                            <medium class="text-danger">{{ $errors->first('summary') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-lg-3 text-lg-right col-form-label">Facebook Link</label>
                        <div class="col-md-9 col-xl-6">
                            <input type="text" name="facebook" value="{{ old('facebook') }}" placeholder="Enter Facebook Link ..." class="form-control"/>
                            <medium class="text-danger" >{{ $errors->first('facebook') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-lg-3 text-lg-right col-form-label">Twitter Link</label>
                        <div class="col-md-9 col-xl-6">
                            <input type="text" name="twitter" value="{{ old('twitter') }}" placeholder="Enter Twitter Link ..." class="form-control"/>
                            <medium class="text-danger" >{{ $errors->first('twitter') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-lg-3 text-lg-right col-form-label">Instagram Link</label>
                        <div class="col-md-9 col-xl-6">
                            <input type="text" name="instagram" value="{{ old('instagram') }}" placeholder="Enter Instagram Link ..." class="form-control"/>
                            <medium class="text-danger" >{{ $errors->first('instagram') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-lg-3 text-lg-right col-form-label">Profile Image</label>
                        <div class="col-md-9 col-xl-6">
                            <input type="file" name="profile_image" class="form-control"/>
                            <medium class="text-danger" >{{ $errors->first('profile_image') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-lg-3 text-lg-right col-form-label">Cover Image</label>
                        <div class="col-md-9 col-xl-6">
                            <input type="file" name="cover_image" class="form-control"/>
                            <medium class="text-danger" >{{ $errors->first('cover_image') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-lg-3 text-lg-right col-form-label">Default Music</label>
                        <div class="col-md-9 col-xl-6">
                            <input type="file" name="default_music" class="form-control"/>
                            <medium class="text-danger" >{{ $errors->first('default_music') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-lg-3 text-lg-right col-form-label">Enter Latitude</label>
                        <div class="col-md-9 col-xl-6">
                            <input type="text" name="lat" placeholder="Enter Latitude Link ..." class="form-control"/>
                            <medium class="text-danger" >{{ $errors->first('lat') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-lg-3 text-lg-right col-form-label">Enter Longitude</label>
                        <div class="col-md-9 col-xl-6">
                            <input type="text" name="lon" placeholder="Enter Longitude ..." class="form-control"/>
                            <medium class="text-danger" >{{ $errors->first('lon') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-0">
                        <label class="col-md-4 col-sm-4 col-form-label">&nbsp;</label>
                        <div class="col-md-8 col-sm-8">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>


                </form>
            </div>
            <!-- end panel-body -->

            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Modal Header</h4>
                        </div>
                        <div class="modal-body">
                            <p>Some text in the modal.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- end panel -->
            @if(Session::get('user_id'))
                <script>
                    $(function() {
                        $('#myModal').modal('show');
                    });
                </script>
            @endif
    @section('page_specific_scripts')
        <script src="{{ asset('assets/admin/js/dobpicker.js') }}"></script>
        <script src="{{ asset('assets/admin/plugins/ckeditor/ckeditor.js') }}"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $.dobPicker({
                    daySelector: '#dobday', /* Required */
                    monthSelector: '#dobmonth', /* Required */
                    yearSelector: '#dobyear', /* Required */
                    dayDefault: 'Day', /* Optional */
                    monthDefault: 'Month', /* Optional */
                    yearDefault: 'Year', /* Optional */
                    minimumAge: 12, /* Optional */
                    maximumAge: 80 /* Optional */
                });
            });
        </script>
    @endsection
@endsection

