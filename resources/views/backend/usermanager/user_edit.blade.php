@extends('backend.layouts.app')
@section('page_title')
    CMS-User-Manager
@endsection

@section('page_specefic_css')
    <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
    <link href="{{ asset('assets/admin/plugins/smartwizard/dist/css/smart_wizard.css') }}" rel="stylesheet" />
    <!-- ================== END PAGE LEVEL STYLE ================== -->
    @endsection
@section('breadcomes')
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashbaord</a></li>
        <li class="breadcrumb-item active">User Manager Edit</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">User Manager Edit <small>header small text goes here...</small></h1>
    <!-- end page-header -->
@endsection

@section('content')
        <form action="{{ route('admin.user-manager.user-update', $data['row']->id) }}" method="POST" enctype="multipart/form-data" name="form-wizard" class="form-control-with-bg">
            @csrf
            <div id="step-1">
                <!-- begin fieldset -->
                <fieldset>
                    <!-- begin row -->
                    <div class="row">
                        <!-- begin col-8 -->
                        <div class="col-xl-8 offset-xl-2">
                            <legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Personal info about yourself</legend>
                            <!-- begin form-group -->
                            <div class="form-group row m-b-10">
                                <label class="col-lg-3 text-lg-right col-form-label">Full Name <span class="text-danger">*</span></label>
                                <div class="col-lg-9 col-xl-6">
                                    <input type="text" name="name" value="{{ isset($data['row'])?$data['row']->name:'' }}" required placeholder="Enter Your Full Name ..." data-parsley-group="step-1" data-parsley-required="true" class="form-control" />
                                </div>
                            </div>
                            <!-- end form-group -->
                            <!-- begin form-group -->
                            <div class="form-group row m-b-10">
                                <label class="col-lg-3 text-lg-right col-form-label">Email <span class="text-danger">*</span></label>
                                <div class="col-lg-9 col-xl-6">
                                    <input type="email" name="email" value="{{ isset($data['row'])?$data['row']->email:'' }}" required placeholder="Enter Your Email ..." data-parsley-group="step-1" data-parsley-required="true" class="form-control" />
                                </div>
                            </div>
                            <!-- end form-group -->
                            <!-- begin form-group -->
                            <div class="form-group row m-b-10">
                                <label class="col-lg-3 text-lg-right col-form-label">Phone No.<span class="text-danger">*</span></label>
                                <div class="col-lg-9 col-xl-6">
                                    <input type="text" name="phone" value="{{ isset($data['row'])?$data['row']->phone:'' }}" required placeholder="Enter Your Phone Number ..." class="form-control" data-parsley-group="step-1" data-parsley-required="true" />
                                </div>
                            </div>

                            <div class="form-group row m-b-10">
                                <label class="col-lg-3 text-lg-right col-form-label">New Password (If you want to change.)</label>
                                <div class="col-lg-9 col-xl-6">
                                    <input type="password" name="password" placeholder="Enter Password ..." class="form-control" data-parsley-group="step-1" data-parsley-required="true" />
                                </div>
                            </div>

                            <div class="form-group row m-b-10">
                                <label class="col-lg-3 text-lg-right col-form-label">Confirm Password<span class="text-danger">*</span></label>
                                <div class="col-lg-9 col-xl-6">
                                    <input type="password" name="confirm_password" placeholder="Enter Confirm Password ..." class="form-control" data-parsley-group="step-1" data-parsley-required="true" />
                                </div>
                            </div>
                            <!-- end form-group -->
                        </div>
                        <!-- end col-8 -->
                    </div>
                    <!-- end row -->
                </fieldset>
                <!-- end fieldset -->
            </div>

            <div class="form-group row m-b-0">
                <label class="col-md-4 col-sm-4 col-form-label">&nbsp;</label>
                <div class="col-md-8 col-sm-8">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </div>
        </form>
        <!-- end wizard-form -->

@section('page_specific_scripts')
    <script src="{{ asset('assets/admin/plugins/ckeditor/ckeditor.js') }}"></script>
    <!-- ================== BEGIN PAGE LEVEL JS ================== -->
    <script src="{{ asset('assets/admin/plugins/parsleyjs/dist/parsley.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/smartwizard/dist/js/jquery.smartWizard.js') }}"></script>
    <script src="{{ asset('assets/admin/js/demo/form-wizards-validation.demo.js') }}"></script>
    <!-- ================== END PAGE LEVEL JS ================== -->
    <script>
        $( "#datepicker" ).datepicker({
            format: "mm/dd/yy",
            weekStart: 0,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            rtl: true,
            orientation: "auto"
        });
    </script>
@endsection
@endsection

