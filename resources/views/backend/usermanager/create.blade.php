@extends('backend.layouts.app')
@section('page_title')
    CMS-User-Manager
@endsection

@section('page_specefic_css')
    <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
    <link href="{{ asset('assets/admin/plugins/smartwizard/dist/css/smart_wizard.css') }}" rel="stylesheet" />
    <!-- ================== END PAGE LEVEL STYLE ================== -->
    @endsection
@section('breadcomes')
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashbaord</a></li>
        <li class="breadcrumb-item active">User Manager Create</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">User Manager Create <small>header small text goes here...</small></h1>
    <!-- end page-header -->
@endsection

@section('content')
        <form action="/" method="POST" name="form-wizard" class="form-control-with-bg">
            <!-- begin wizard -->
            <div id="wizard">
                <!-- begin wizard-step -->
                <ul>
                    <li>
                        <a href="#step-1">
                            <span class="number">1</span>
                            <span class="info">
									Personal Info
									<small>Name, Email, Phone and Password</small>
								</span>
                        </a>
                    </li>
                    <li>
                        <a href="#step-2">
                            <span class="number">2</span>
                            <span class="info">
									Profile Info
									<small>Email and phone no. is required</small>
								</span>
                        </a>
                    </li>
                    <li>
                        <a href="#step-3">
                            <span class="number">3</span>
                            <span class="info">
									Media Section
									<small>Enter Profile Image, Cover Image and Default Music</small>
								</span>
                        </a>
                    </li>
                    <li>
                        <a href="#step-4">
                            <span class="number">4</span>
                            <span class="info">
									Social Links
									<small>Complete Registration</small>
								</span>
                        </a>
                    </li>
                </ul>
                <!-- end wizard-step -->
                <!-- begin wizard-content -->
                <div>
                    <!-- begin step-1 -->
                    <div id="step-1">
                        <!-- begin fieldset -->
                        <fieldset>
                            <!-- begin row -->
                            <div class="row">
                                <!-- begin col-8 -->
                                <div class="col-xl-8 offset-xl-2">
                                    <legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Personal info about yourself</legend>
                                    <!-- begin form-group -->
                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label">Full Name <span class="text-danger">*</span></label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input type="text" name="name" placeholder="Enter Your Full Name ..." data-parsley-group="step-1" data-parsley-required="true" class="form-control" />
                                        </div>
                                    </div>
                                    <!-- end form-group -->
                                    <!-- begin form-group -->
                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label">Email <span class="text-danger">*</span></label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input type="email" name="email" placeholder="Enter Your Email ..." data-parsley-group="step-1" data-parsley-required="true" class="form-control" />
                                        </div>
                                    </div>
                                    <!-- end form-group -->
                                    <!-- begin form-group -->
                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label">Phone No.<span class="text-danger">*</span></label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input type="text" name="phone" placeholder="Enter Your Phone Number ..." class="form-control" data-parsley-group="step-1" data-parsley-required="true" />
                                        </div>
                                    </div>

                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label">Password<span class="text-danger">*</span></label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input type="password" name="password" placeholder="Enter Password ..." class="form-control" data-parsley-group="step-1" data-parsley-required="true" />
                                        </div>
                                    </div>

                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label">Confirm Password<span class="text-danger">*</span></label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input type="password" name="confirm_password" placeholder="Enter Confirm Password ..." class="form-control" data-parsley-group="step-1" data-parsley-required="true" />
                                        </div>
                                    </div>
                                    <!-- end form-group -->
                                </div>
                                <!-- end col-8 -->
                            </div>
                            <!-- end row -->
                        </fieldset>
                        <!-- end fieldset -->
                    </div>
                    <!-- end step-1 -->
                    <!-- begin step-2 -->
                    <div id="step-2">
                        <!-- begin fieldset -->
                        <fieldset>
                            <!-- begin row -->
                            <div class="row">
                                <!-- begin col-8 -->
                                <div class="col-xl-8 offset-xl-2">
                                    <legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">You contact info, so that we can easily reach you</legend>
                                    <!-- begin form-group -->

                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label">Nick Name <span class="text-danger">*</span></label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input type="text" name="nickname" placeholder="Enter your Nick Name ..." data-parsley-group="step-2" data-parsley-required="true" class="form-control" />
                                        </div>
                                    </div>

                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label">Gender<span class="text-danger">&nbsp;</span></label>
                                        <div class="col-lg-9 col-xl-6">
                                            <div class="row row-space-6">
                                                <div class="col-4">
                                                    <select class="form-control" name="gender">
                                                        <option>Male</option>
                                                        <option>Female</option>
                                                        <option>Others</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label">Marital Status<span class="text-danger">&nbsp;</span></label>
                                        <div class="col-lg-9 col-xl-6">
                                            <div class="row row-space-6">
                                                <div class="col-4">
                                                    <select class="form-control" name="marital_status">
                                                        <option>Married</option>
                                                        <option>Un-marriad</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label">Date of Birth <span class="text-danger">&nbsp;</span></label>
                                        <div class="col-lg-9 col-xl-6">
                                            <div class="row row-space-6">
                                                <div class="col-4">
                                                    <select class="form-control" name="dob">
                                                        <option>-- Year --</option>
                                                    </select>
                                                </div>
                                                <div class="col-4">
                                                    <select class="form-control" name="month">
                                                        <option>-- Month --</option>
                                                        <option>-- Januaru --</option>
                                                        <option>-- Februry --</option>
                                                        <option>-- March --</option>
                                                        <option>-- April --</option>
                                                        <option>-- May --</option>
                                                        <option>-- June --</option>
                                                        <option>-- July --</option>
                                                        <option>-- August --</option>
                                                        <option>-- September --</option>
                                                        <option>-- October --</option>
                                                        <option>-- November --</option>
                                                        <option>-- December --</option>
                                                    </select>
                                                </div>
                                                <div class="col-4">
                                                    <select class="form-control" name="day">
                                                        <option>-- Day --</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label">Phone Number <span class="text-danger">*</span></label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input type="number" name="phone" placeholder="123-456-7890" data-parsley-group="step-2" data-parsley-required="true" data-parsley-type="number" class="form-control" />
                                        </div>
                                    </div>
                                    <!-- end form-group -->
                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label">Profession <span class="text-danger">*</span></label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input type="text" name="profession" placeholder="Enter your Profession ..." data-parsley-group="step-2" data-parsley-required="true" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label">Address Name <span class="text-danger">*</span></label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input type="text" name="address_string" placeholder="Enter your Address ..." data-parsley-group="step-2" data-parsley-required="true" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label">Street <span class="text-danger">*</span></label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input type="text" name="street" placeholder="Enter your Street Name ..." data-parsley-group="step-2" data-parsley-required="true" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label">Sub Urban <span class="text-danger">*</span></label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input type="text" name="suburb" placeholder="Enter your Sub Urban ..." data-parsley-group="step-2" data-parsley-required="true" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label">Post Code <span class="text-danger">*</span></label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input type="text" name="postcode" placeholder="Enter your Post Code ..." data-parsley-group="step-2" data-parsley-required="true" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label">State <span class="text-danger">*</span></label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input type="text" name="state" placeholder="Enter your State ..." data-parsley-group="step-2" data-parsley-required="true" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label">Country <span class="text-danger">*</span></label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input type="text" name="country" placeholder="Enter your Country Name ..." data-parsley-group="step-2" data-parsley-required="true" class="form-control" />
                                        </div>
                                    </div>

                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label">Description</label>
                                        <div class="col-md-9">
                                            <textarea class="form-control ckeditor" id="editor1" name="summary" rows="20">{{ old('summary') }}</textarea>
                                            <medium class="text-danger">{{ $errors->first('summary') }}</medium>
                                        </div>
                                    </div>
                                    <!-- end form-group -->
                                </div>
                                <!-- end col-8 -->
                            </div>
                            <!-- end row -->
                        </fieldset>
                        <!-- end fieldset -->
                    </div>
                    <!-- end step-2 -->
                    <!-- begin step-3 -->
                    <div id="step-3">
                        <!-- begin fieldset -->
                        <fieldset>
                            <!-- begin row -->
                            <div class="row">
                                <!-- begin col-8 -->
                                <div class="col-xl-8 offset-xl-2">
                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label"> <span class="text-danger">*</span> Profile Image</label>
                                        <div class="col-md-9 col-xl-6">
                                            <input type="file" name="profile_image" class="form-control"/>
                                            <medium class="text-danger" >{{ $errors->first('profile_image') }}</medium>
                                        </div>
                                    </div>

                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label"> <span class="text-danger">*</span> Cover Image</label>
                                        <div class="col-md-9 col-xl-6">
                                            <input type="file" name="cover_image" class="form-control"/>
                                            <medium class="text-danger" >{{ $errors->first('cover_image') }}</medium>
                                        </div>
                                    </div>

                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label"> <span class="text-danger">*</span> Default Music</label>
                                        <div class="col-md-9 col-xl-6">
                                            <input type="file" name="default_music" class="form-control"/>
                                            <medium class="text-danger" >{{ $errors->first('default_music') }}</medium>
                                        </div>
                                    </div>

                                </div>
                                <!-- end col-8 -->
                            </div>
                            <!-- end row -->
                        </fieldset>
                        <!-- end fieldset -->
                    </div>
                    <!-- end step-3 -->
                    <!-- begin step-4 -->
                    <div id="step-4">
                        <!-- begin fieldset -->
                        <fieldset>
                            <!-- begin row -->
                            <div class="row">
                                <!-- begin col-8 -->
                                <div class="col-xl-8 offset-xl-2">
                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label"> <span class="text-danger">*</span> Facebook Link</label>
                                        <div class="col-md-9 col-xl-6">
                                            <input type="text" name="facebook" placeholder="Enter Facebook Link ..." class="form-control"/>
                                            <medium class="text-danger" >{{ $errors->first('facebook') }}</medium>
                                        </div>
                                    </div>

                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label"> <span class="text-danger">*</span> Twitter Link</label>
                                        <div class="col-md-9 col-xl-6">
                                            <input type="text" name="twitter" placeholder="Enter Twitter Link ..." class="form-control"/>
                                            <medium class="text-danger" >{{ $errors->first('twitter') }}</medium>
                                        </div>
                                    </div>

                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label"> <span class="text-danger">*</span> Instagram Link</label>
                                        <div class="col-md-9 col-xl-6">
                                            <input type="text" name="instagram" placeholder="Enter Instagram Link ..." class="form-control"/>
                                            <medium class="text-danger" >{{ $errors->first('instagram') }}</medium>
                                        </div>
                                    </div>

                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label"> <span class="text-danger">*</span> Enter Latitude</label>
                                        <div class="col-md-9 col-xl-6">
                                            <input type="text" name="lat" placeholder="Enter Latitude Link ..." class="form-control"/>
                                            <medium class="text-danger" >{{ $errors->first('lat') }}</medium>
                                        </div>
                                    </div>

                                    <div class="form-group row m-b-10">
                                        <label class="col-lg-3 text-lg-right col-form-label"> <span class="text-danger">*</span>Enter Longitude</label>
                                        <div class="col-md-9 col-xl-6">
                                            <input type="text" name="lon" placeholder="Enter Longitude ..." class="form-control"/>
                                            <medium class="text-danger" >{{ $errors->first('lon') }}</medium>
                                        </div>
                                    </div>

                                    <div class="form-group row m-b-0">
                                        <label class="col-md-4 col-sm-4 col-form-label">&nbsp;</label>
                                        <div class="col-md-8 col-sm-8">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>



                                </div>
                                <!-- end col-8 -->
                            </div>
                            <!-- end row -->
                        </fieldset>
                        <!-- end fieldset -->
                    </div>
                    <!-- end step-4 -->
                </div>
                <!-- end wizard-content -->
            </div>
            <!-- end wizard -->
        </form>
        <!-- end wizard-form -->

@section('page_specific_scripts')
    <script src="{{ asset('assets/admin/plugins/ckeditor/ckeditor.js') }}"></script>
    <!-- ================== BEGIN PAGE LEVEL JS ================== -->
    <script src="{{ asset('assets/admin/plugins/parsleyjs/dist/parsley.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/smartwizard/dist/js/jquery.smartWizard.js') }}"></script>
    <script src="{{ asset('assets/admin/js/demo/form-wizards-validation.demo.js') }}"></script>
    <!-- ================== END PAGE LEVEL JS ================== -->
    <script>
        $( "#datepicker" ).datepicker({
            format: "mm/dd/yy",
            weekStart: 0,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            rtl: true,
            orientation: "auto"
        });
    </script>
@endsection
@endsection

