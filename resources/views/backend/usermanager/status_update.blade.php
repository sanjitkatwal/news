@extends('backend.layouts.app')
@section('page_title')
    CMS-User-Status-Manager
@endsection

@section('breadcomes')
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashbaord</a></li>
        <li class="breadcrumb-item active">User Status Update</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">User Status Update<small>header small text goes here...</small></h1>
    <!-- end page-header -->
@endsection

@section('content')
    <!-- begin panel -->
    <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
        <!-- begin panel-heading -->
        <div class="panel-heading ui-sortable-handle">
            <h4 class="panel-title">Form Controls</h4>
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
        </div>
        <!-- end panel-heading -->
        <!-- begin panel-body -->
        <div class="panel-body">
            <form action="{{ route('admin.user-profile.update-status', $data['row']->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group row m-b-10">
                    <label class="col-form-label col-md-3">Update Status</label>
                    <div class="col-md-3">
                        <select name="status" class="form-control mb-9">
                            <option value="" select>Select One Status</option>
                            <option value="0" {{ isset($data['row'])&& $data['row']->status == '0'?'selected':'' }}>Pending</option>
                            <option value="1" {{ isset($data['row'])&& $data['row']->status == '1'?'selected':'' }}>Approved</option>
                            <option value="2" {{ isset($data['row'])&& $data['row']->status == '2'?'selected':'' }}>Rejected</option>
                            <option value="3" {{ isset($data['row'])&& $data['row']->status == '3'?'selected':'' }}>Postpone</option>

                        </select>
                        <medium class="text-danger" >{{ $errors->first('status') }}</medium>

                    </div>
                        <div class="form-group row m-b-0">
                            <label class="col-md-4 col-sm-4 col-form-label">&nbsp;</label>
                            <div class="col-md-8 col-sm-8">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>

                </div>

            </form>
        </div>
        <!-- end panel-body -->

    </div>
    <!-- end panel -->

@endsection
@section('page_specific_scripts')
    <script src="{{ asset('assets/admin/plugins/ckeditor/ckeditor.js') }}"></script>
@endsection
