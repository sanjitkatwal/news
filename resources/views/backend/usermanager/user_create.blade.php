@extends('backend.layouts.app')
@section('page_title')
    CMS-User-Manager
@endsection

@section('page_specefic_css')
    <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
    <link href="{{ asset('assets/admin/plugins/smartwizard/dist/css/smart_wizard.css') }}" rel="stylesheet" />
    <!-- ================== END PAGE LEVEL STYLE ================== -->
    @endsection
@section('breadcomes')
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashbaord</a></li>
        <li class="breadcrumb-item active">User Manager Create</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">User Manager Create <small>header small text goes here...</small></h1>
    <!-- end page-header -->
@endsection

@section('content')
        <form action="{{ route('admin.user-manager.store') }}" method="POST" enctype="multipart/form-data" name="form-wizard" class="form-control-with-bg">
            @csrf
            <div id="step-1">
                <!-- begin fieldset -->
                <fieldset>
                    <!-- begin row -->
                    <div class="row">
                        <!-- begin col-8 -->
                        <div class="col-xl-8 offset-xl-2">
                            <legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Personal info about yourself</legend>
                            <!-- begin form-group -->
                            <div class="form-group row m-b-10">
                                <label class="col-lg-3 text-lg-right col-form-label">Full Name <span class="text-danger">*</span></label>
                                <div class="col-lg-9 col-xl-6">
                                    <input type="text" name="name" value="{{ old('name') }}" placeholder="Enter Your Full Name ..." data-parsley-group="step-1" data-parsley-required="true" class="form-control" />
                                    <medium class="text-danger" >{{ $errors->first('name') }}</medium>
                                </div>
                            </div>
                            <!-- end form-group -->
                            <!-- begin form-group -->
                            <div class="form-group row m-b-10">
                                <label class="col-lg-3 text-lg-right col-form-label">Email <span class="text-danger">*</span></label>
                                <div class="col-lg-9 col-xl-6">
                                    <input type="email" value="{{ old('email') }}" name="email" required placeholder="Enter Your Email ..." data-parsley-group="step-1" data-parsley-required="true" class="form-control" />
                                    <medium class="text-danger" >{{ $errors->first('email') }}</medium>
                                </div>
                            </div>
                            <!-- end form-group -->
                            <!-- begin form-group -->
                            <div class="form-group row m-b-10">
                                <label class="col-lg-3 text-lg-right col-form-label">Phone No.</label>
                                <div class="col-lg-9 col-xl-6">
                                    <input type="text" value="{{ old('phone') }}" name="phone" placeholder="Enter Your Phone Number ..." class="form-control" data-parsley-group="step-1" data-parsley-required="true" />
                                    <medium class="text-danger" >{{ $errors->first('phone') }}</medium>
                                </div>
                            </div>

                            <div class="form-group row m-b-10">
                                <label class="col-lg-3 text-lg-right col-form-label">Password<span class="text-danger">*</span></label>
                                <div class="col-lg-9 col-xl-6">
                                    <input type="password" value="{{ old('password') }}" name="password" required placeholder="Enter Password ..." class="form-control" data-parsley-group="step-1" data-parsley-required="true" />
                                    <medium class="text-danger" >{{ $errors->first('password') }}</medium>
                                </div>
                            </div>

                            <div class="form-group row m-b-10">
                                <label class="col-lg-3 text-lg-right col-form-label">Confirm Password<span class="text-danger">*</span></label>
                                <div class="col-lg-9 col-xl-6">
                                    <input type="password" value="{{ old('password_confirmation') }}" name="password_confirmation" placeholder="Enter Confirm Password ..." class="form-control" data-parsley-group="step-1" data-parsley-required="true" />
                                    <medium class="text-danger" >{{ $errors->first('password_confirmation') }}</medium>
                                </div>
                            </div>
                            <!-- end form-group -->
                        </div>
                        <!-- end col-8 -->
                    </div>
                    <!-- end row -->
                </fieldset>
                <!-- end fieldset -->
            </div>

            <div class="form-group row m-b-0">
                <label class="col-md-4 col-sm-4 col-form-label">&nbsp;</label>
                <div class="col-md-8 col-sm-8">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
        <!-- end wizard-form -->

@section('page_specific_scripts')
    <script src="{{ asset('assets/admin/plugins/ckeditor/ckeditor.js') }}"></script>
    <!-- ================== BEGIN PAGE LEVEL JS ================== -->
    <script src="{{ asset('assets/admin/plugins/parsleyjs/dist/parsley.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/smartwizard/dist/js/jquery.smartWizard.js') }}"></script>
    <script src="{{ asset('assets/admin/js/demo/form-wizards-validation.demo.js') }}"></script>
    <!-- ================== END PAGE LEVEL JS ================== -->
    <script>
        $( "#datepicker" ).datepicker({
            format: "mm/dd/yy",
            weekStart: 0,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            rtl: true,
            orientation: "auto"
        });
    </script>
@endsection
@endsection

