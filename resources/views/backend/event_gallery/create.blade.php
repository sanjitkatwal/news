@extends('backend.layouts.app')
@section('page_title')
    CMS-Event-Gallery-Manager
@endsection

@section('breadcomes')
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashbaord</a></li>
        <li class="breadcrumb-item active">Event Gallery Create</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Event Gallery Create <small>header small text goes here...</small></h1>
    <!-- end page-header -->
@endsection

@section('content')
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
            <!-- begin panel-heading -->
            <div class="panel-heading ui-sortable-handle">
                <h4 class="panel-title">Form Controls</h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <!-- end panel-heading -->
            <!-- begin panel-body -->
            <div class="panel-body">
                <form action="{{ route('admin.event-galleries.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row m-b-10">
                            <label class="col-form-label col-md-3">Event Name</label>
                        <div class="col-md-9">
                            <select name="event_id" class="form-control mb-3">
                            @if(count($data['event']) > 0)
                                <option value="" select>Select Your Event Name</option>
                                @foreach($data['event'] as $event)
                                    <option value="{{ $event->id }}">{!! $event->event_title !!}</option>
                                @endforeach

                                @else
                                    <option>No Post Is Available.</option>
                                @endif
                            </select>
                            <medium class="text-danger" >{{ $errors->first('business_id') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-10">
                        <label class="col-form-label col-md-3">Media Type</label>
                        <div class="col-md-9">
                            <select name="media_type" class="form-control mb-3">
                                    <option value="" selected>Select Media Type</option>
                                    <option value="Image">Image</option>
                                    <option value="Video">Video</option>
                                    <option value="Audio">Audio</option>
                            </select>
                            <medium class="text-danger" >{{ $errors->first('media_type') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Select File</label>
                        <div class="col-md-9">
                            <input type="file" name="filepath" class="form-control"/>
                            <medium class="text-danger" >{{ $errors->first('filepath') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Caption</label>
                        <div class="col-md-9">
                            <input type="text" name="caption" class="form-control" placeholder="Enter Caption ....." />
                            <medium class="text-danger" >{{ $errors->first('caption') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Description</label>
                        <div class="col-md-9">
                            <textarea class="form-control ckeditor" id="editor1" name="description" rows="20"></textarea>
                            <medium class="text-danger" >{{ $errors->first('description') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-0">
                        <label class="col-md-4 col-sm-4 col-form-label">&nbsp;</label>
                        <div class="col-md-8 col-sm-8">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>


                </form>
            </div>
            <!-- end panel-body -->

        </div>
        <!-- end panel -->

@endsection
@section('page_specific_scripts')
    <script src="{{ asset('assets/admin/plugins/ckeditor/ckeditor.js') }}"></script>
@endsection
