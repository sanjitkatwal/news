@extends('backend.layouts.app')
@section('page_title')
    CMS-Event-Gallery-Manager
@endsection

@section('breadcomes')
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashbaord</a></li>
        <li class="breadcrumb-item active">Event Gallery Edit</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Event Gallery Edit <small>header small text goes here...</small></h1>
    <!-- end page-header -->
@endsection

@section('content')
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
            <!-- begin panel-heading -->
            <div class="panel-heading ui-sortable-handle">
                <h4 class="panel-title">Edit Control Form</h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <!-- end panel-heading -->
            <!-- begin panel-body -->
            <div class="panel-body">
                <form action="{{ route('admin.event-galleries.update', $data['row']->id) }}" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group row m-b-10">
                        <label class="col-form-label col-md-3">Event Name</label>
                        <div class="col-md-9">
                            <select name="event_id" class="form-control mb-3">
                                @if(count($data['event']) > 0)
                                    <option value="" select>Select Your Post Name</option>
                                    @foreach($data['event'] as $event)
                                        <option value="{{ $event->id }}"
                                            {{isset($data['row']) && $data['row']->event_id == $event->id?'selected':''}}>
                                            {!! $event->event_title !!}</option>
                                    @endforeach

                                @else
                                    <option>No Event Name Is Available.</option>
                                @endif
                            </select>
                            <medium class="text-danger" >{{ $errors->first('event_id') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-10">
                        <label class="col-form-label col-md-3">Media Type</label>
                        <div class="col-md-9">
                            <select name="media_type" class="form-control mb-3">
                                <option value="" selected>Select Media Type</option>
                                <option value="Image" {{ isset($data['row'])&& $data['row']->media_type == 'Image'?'selected':'' }}>Image</option>
                                <option value="Video" {{ isset($data['row'])&& $data['row']->media_type == 'Video'?'selected':'' }}>Vidoe</option>
                                <option value="Audio" {{ isset($data['row'])&& $data['row']->media_type == 'Audio'?'selected':'' }}>Audio</option>
                            </select>
                            <medium class="text-danger" >{{ $errors->first('media_type') }}</medium>
                        </div>
                    </div>

                    @if (isset($data['row']))
                        @if($data['row']->filepath)
                            <div class="form-group row m-b-15">
                                <label class="col-form-label col-md-3">Your Old File</label>
                                <div class="col-md-9">
                                    @if($data['row']->media_type == 'Image')
                                    <img src="{{ asset('storage/'.$data['row']->filepath) }}" alt="No Image Available" style="width: 60px; height: 70px;">
                                    @elseif($data['row']->media_type == 'Audio')
                                        <audio controls style="height:35px; background-color: gray; width: 130px;">
                                            <source src="{{ asset('storage/'.$data['row']->filepath) }}">
                                        </audio>
                                    @else($data['row']->media_type == 'Video')
                                        <video width="110" height="90" controls>
                                            <source src="{{ asset('storage/'.$data['row']->filepath) }}" type="video/mp4">
                                        </video>
                                    @endif
                                        <medium class="text-danger" >{{ $errors->first('filepath') }}</medium>
                                </div>
                            </div>
                        @else
                            <div class="form-group row m-b-15">
                                <label class="col-form-label col-md-3">Select File</label>
                                <div class="col-md-9">
                                    <p>No Image Found!</p>
                                </div>
                            </div>
                        @endif

                    @endif
                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Select New File</label>
                        <div class="col-md-9">
                            <input type="file" name="filepath" class="form-control"/>
                            <medium class="text-danger" >{{ $errors->first('filepath') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Caption</label>
                        <div class="col-md-9">
                            <input type="text" name="caption" value="{{ $data['row']->caption }}" class="form-control" placeholder="Enter Caption ....." />
                            <medium class="text-danger" >{{ $errors->first('caption') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Description</label>
                        <div class="col-md-9">
                            <textarea class="form-control ckeditor" id="editor1" name="description" rows="20">
                                {{ $data['row']->description }}
                            </textarea>
                            <medium class="text-danger" >{{ $errors->first('description') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-0">
                        <label class="col-md-4 col-sm-4 col-form-label">&nbsp;</label>
                        <div class="col-md-8 col-sm-8">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>


                </form>
            </div>
            <!-- end panel-body -->

        </div>
        <!-- end panel -->

@endsection

@section('page_specific_scripts')
    <script src="{{ asset('assets/admin/plugins/ckeditor/ckeditor.js') }}"></script>
@endsection
