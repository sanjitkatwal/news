@extends('backend.layouts.app')
@section('page_title')
    Error-500
@endsection
@section('breadcomes')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="#"> &nbsp; 500 Error</a></li>
        </ol>
    </section>

@endsection

@section('content')
      <!-- Main content -->
    <section class="content">

        <div class="error-page">
            <h2 class="headline text-red">500</h2>

            <div class="error-content">
                <h3><i class="fa fa-warning text-red"></i> Oops! Something went wrong.</h3>

                <p>
                    We will work on fixing that right away.
                    Meanwhile, you may <a href="{{ route('admin.dashboard') }}">return to dashboard</a> or try using the search form.
                </p>

                <form class="search-form" action="{{ route('admin.dashboard') }}">
                    <div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Search">

                        <div class="input-group-btn">
                            <button type="submit" name="submit" class="btn btn-danger btn-flat"><i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.input-group -->
                </form>
            </div>

            <br>
            <br>
            <br>
            <div class="text-center">
                <a href="{{ route('admin.dashboard') }}" class="btn btn-primary">
                    <i class="fa fa-dashboard"></i>
                    Dashboard
                </a>
            </div>
        </div>
        <!-- /.error-page -->
    </section>
    <!-- /.content -->
@endsection

