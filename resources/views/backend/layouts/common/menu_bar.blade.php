<div id="sidebar" class="sidebar">
    <!-- begin sidebar scrollbar -->
    <div data-scrollbar="true" data-height="100%">
        <!-- begin sidebar user -->
        <ul class="nav">
            <li class="nav-profile">
                <a href="javascript:;" data-toggle="nav-profile">
                    <div class="cover with-shadow"></div>
                    <div class="image">
                        <img src="{{ asset('admin/assets/img/user/user-13.jpg') }}" alt="" />
                    </div>
                    <div class="info">
                        <b class="caret pull-right"></b>
                        Admin
                        <small>Backend developer</small>
                    </div>
                </a>
            </li>
            <li>
                <ul class="nav nav-profile">
                    <li><a href="javascript:;"><i class="fa fa-cog"></i> Settings</a></li>
                    <li><a href="javascript:;"><i class="fa fa-pencil-alt"></i> Send Feedback</a></li>
                    <li><a href="javascript:;"><i class="fa fa-question-circle"></i> Helps</a></li>
                </ul>
            </li>
        </ul>
        <!-- end sidebar user -->
        <!-- begin sidebar nav -->
        <ul class="nav">
            <li class="nav-header">Navigation</li>
            <li class="has-sub">
                <a href="javascript:;">
                    <b class="caret"></b>
                    <i class="fa fa-th-large"></i>
                    <span>Dashboard</span>
                </a>
                <ul class="sub-menu">
                    <li class="active"><a href="{{ route('admin.report') }}">Report</a></li>
                    <li><a href="#">Dashboard v2</a></li>
                    <li><a href="#">Dashboard v3</a></li>
                </ul>
            </li>

            <li class="{{ Request::is('admin/user*')?'active':'' }} has-sub open">
                <a href="javascript:;">
                    <b class="caret"></b>
                    <i class="fa fa-user"></i>
                    <span>User Manager</span>
                </a>
                <ul class="sub-menu">
                    <li class="{{ Request::is('admin/user*')?'active':'' }} has-sub open">
                        <a href="javascript:;">
                            <b class="caret"></b>
                            <i class="fa fa-user"></i>
                            <span>User</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="{{ Request::is('admin/user-manager/list')?'active':'' }}">
                                <a href="{{ route('admin.user-manager.list') }}">List User</a>
                            </li>
                            <li class="{{ Request::is('admin/user-manager/deleted-list')?'active':'' }}">
                                <a href="{{ route('admin.user-manager.deleted-list') }}">List Deleted User</a>
                            </li>
                            <li class="{{ Request::is('admin/user-manager/create')?'active':'' }}">
                                <a href="{{ route('admin.user-manager.create') }}">Add User</a>
                            </li>
                            <li class="{{ Request::is('admin/user-profile/create')?'active':'' }}">
                                <a href="{{ route('admin.user-profile.create') }}">Add User Profile</a>
                            </li>
                        </ul>
                    </li>

                    <li class="{{ Request::is('admin/user-music*')?'active':'' }} has-sub open">
                        <a href="javascript:;">
                            <b class="caret"></b>
                            <i class="fa fa-music"></i>
                            <span>Music Manager</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="{{ Request::is('admin/user-music/list')?'active':'' }}">
                                <a href="{{ route('admin.user-music.list') }}">List Music</a>
                            </li>
                            <li class="{{ Request::is('admin/user-music/create')?'active':'' }}">
                                <a href="{{ route('admin.user-music.create') }}">Add Music</a>
                            </li>
                        </ul>
                    </li>


                </ul>
            </li>

            <li class="{{ Request::is('admin/post*')?'active':'' }} has-sub open">
                <a href="javascript:;">
                    <b class="caret"></b>
                    <i class="fa fa-th-large"></i>
                    <span>Post Manager</span>
                </a>
                <ul class="sub-menu">
                    <li class="{{ Request::is('admin/post*')?'active':'' }} has-sub open">
                        <a href="javascript:;">
                            <b class="caret"></b>
                            <i class="fa fa-blog"></i>
                            <span>Post</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="{{ Request::is('admin/post-manager/list')?'active':'' }}">
                                <a href="{{ route('admin.post-manager.list') }}">List Post</a>
                            </li>
                            <li class="{{ Request::is('admin/post-manager/create')?'active':'' }}">
                                <a href="{{ route('admin.post-manager.create') }}">Add Post</a>
                            </li>
                        </ul>
                    </li>

                    <li class="{{ Request::is('admin/post*')?'active':'' }} has-sub open">
                        <a href="javascript:;">
                            <b class="caret"></b>
                            <i class="fa fa-image"></i>
                            <span>Gallery Manager</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="{{ Request::is('admin/post-galleries/list')?'active':'' }}">
                                <a href="{{ route('admin.post-galleries.list') }}">List Gallery</a>
                            </li>
                            <li class="{{ Request::is('admin/post-galleries/create')?'active':'' }}">
                                <a href="{{ route('admin.post-galleries.create') }}">Add Gallery</a>
                            </li>
                        </ul>
                    </li>


                </ul>
            </li>

            <li class="{{ Request::is('admin/event*')?'active':'' }} has-sub open">
                <a href="javascript:;">
                    <b class="caret"></b>
                    <i class="fa fa-th-large"></i>
                    <span>Event Manager</span>
                </a>
                <ul class="sub-menu">
                    <li class="{{ Request::is('admin/event*')?'active':'' }} has-sub open">
                        <a href="javascript:;">
                            <b class="caret"></b>
                            <i class="fa fa-blog"></i>
                            <span>Event</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="{{ Request::is('admin/event-manager/list')?'active':'' }}">
                                <a href="{{ route('admin.event-manager.list') }}">List Event</a>
                            </li>
                            <li class="{{ Request::is('admin/event-manager/create')?'active':'' }}">
                                <a href="{{ route('admin.event-manager.create') }}">Add Event</a>
                            </li>
                        </ul>
                    </li>

                    <li class="{{ Request::is('admin/event*')?'active':'' }} has-sub open">
                        <a href="javascript:;">
                            <b class="caret"></b>
                            <i class="fa fa-image"></i>
                            <span>Gallery Manager</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="{{ Request::is('admin/event-galleries/list')?'active':'' }}">
                                <a href="{{ route('admin.event-galleries.list') }}">List Gallery</a>
                            </li>
                            <li class="{{ Request::is('admin/event-galleries/create')?'active':'' }}">
                                <a href="{{ route('admin.event-galleries.create') }}">Add Gallery</a>
                            </li>
                        </ul>
                    </li>


                </ul>
            </li>

            <li class="{{ Request::is('admin/business*')?'active':'' }} has-sub open">
                <a href="javascript:;">
                    <b class="caret"></b>
                    <i class="fa fa-th-large"></i>
                    <span>Business Manager</span>
                </a>
                <ul class="sub-menu">
                    <li class="{{ Request::is('admin/business-manager*')?'active':'' }} has-sub open">
                        <a href="javascript:;">
                            <b class="caret"></b>
                            <i class="fa fa-blog"></i>
                            <span>Business</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="{{ Request::is('admin/business-manager/list')?'active':'' }}">
                                <a href="{{ route('admin.business-manager.list') }}">List Business</a>
                            </li>
                            <li class="{{ Request::is('admin/business-manager/create')?'active':'' }}">
                                <a href="{{ route('admin.business-manager.create') }}">Add Business</a>
                            </li>
                        </ul>
                    </li>

                    <li class="{{ Request::is('admin/business-gallery*')?'active':'' }} has-sub open">
                        <a href="javascript:;">
                            <b class="caret"></b>
                            <i class="fa fa-image"></i>
                            <span>Gallery</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="{{ Request::is('admin/business-gallery/list')?'active':'' }}">
                                <a href="{{ route('admin.business-gallery.list') }}">List Gallery</a>
                            </li>
                            <li class="{{ Request::is('admin/business-gallery/create')?'active':'' }}">
                                <a href="{{ route('admin.business-gallery.create') }}">Add Gallery</a>
                            </li>
                        </ul>
                    </li>

                    <li class=" {{ Request::is('admin/business-music*')?'active':'' }} has-sub open">
                        <a href="javascript:;">
                            <b class="caret"></b>
                            <i class="fa fa-th-large"></i>
                            <span>Music</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="{{ Request::is('admin/business-music/list')?'active':'' }}"><a href="{{ route('admin.business-music.list') }}">Music List</a></li>
                            <li class="{{ Request::is('admin/business-music/create')?'active':'' }}"><a href="{{ route('admin.business-music.create') }}">Add Music</a></li>
                            <li><a href="#">Manage</a></li>
                        </ul>
                    </li>

                    <li class=" {{ Request::is('admin/business-portfolio*')?'active':'' }} has-sub open">
                        <a href="javascript:;">
                            <b class="caret"></b>
                            <i class="fa fa-th-large"></i>
                            <span>Portfolio</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="{{ Request::is('admin/business-portfolio/list')?'active':'' }}"><a href="{{ route('admin.business-portfolio.list') }}">List Portfolio</a></li>
                            <li class="{{ Request::is('admin/business-portfolio/create')?'active':'' }}"><a href="{{ route('admin.business-portfolio.create') }}">Add Portfolio</a></li>
                            <li><a href="#">Manage</a></li>
                        </ul>
                    </li>

                    <li class=" {{ Request::is('admin/business-offer*')?'active':'' }} has-sub open">
                        <a href="javascript:;">
                            <b class="caret"></b>
                            <i class="fa fa-th-large"></i>
                            <span>Offer</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="{{ Request::is('admin/business-offer/list')?'active':'' }}"><a href="{{ route('admin.business-offer.list') }}">List Offers</a></li>
                            <li class="{{ Request::is('admin/business-offer/create')?'active':'' }}"><a href="{{ route('admin.business-offer.create') }}">Add</a></li>
                            <li><a href="#">Manage</a></li>
                        </ul>
                    </li>


                </ul>
            </li>



            {{--<li class=" {{ Request::is('admin/event*')?'active':'' }} has-sub open">
                <a href="javascript:;">
                    <b class="caret"></b>
                    <i class="fa fa-th-large"></i>
                    <span>Event Manager</span>
                </a>
                <ul class="sub-menu">
                    <li class="{{ Request::is('admin/event/list')?'active':'' }}"><a href="{{ route('admin.event.list') }}">List Event</a></li>
                    <li class="{{ Request::is('admin/event/create')?'active':'' }}"><a href="{{ route('admin.event.create') }}">Add</a></li>
                    <li><a href="#">Manage</a></li>
                </ul>
            </li>--}}

            <!-- begin sidebar minify button -->
            <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
            <!-- end sidebar minify button -->
        </ul>
        <!-- end sidebar nav -->
    </div>
    <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>
