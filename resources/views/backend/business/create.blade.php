@extends('backend.layouts.app')
@section('page_title')
    CMS-Business-Manager
@endsection

@section('breadcomes')
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashbaord</a></li>
        <li class="breadcrumb-item active">Business Create</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Business Create <small>header small text goes here...</small></h1>
    <!-- end page-header -->
@endsection

@section('content')
            <!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
    <!-- begin panel-heading -->
    <div class="panel-heading ui-sortable-handle">
        <h4 class="panel-title">Form Controls</h4>
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
    </div>
    <!-- end panel-heading -->
    <!-- begin panel-body -->
    <div class="panel-body">
        <form action="{{ route('admin.business-manager.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">Business Name</label>
                <div class="col-md-9">
                    <input type="text" name="business_name" value="{{ old('business_name') }}" class="form-control" placeholder="Enter Business Name ....." />
                    <medium class="text-danger" >{{ $errors->first('business_name') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">Business Type</label>
                <div class="col-md-9">
                    <input type="text" name="business_type" class="form-control" placeholder="Enter Business Type ....." />
                    <medium class="text-danger" >{{ $errors->first('business_type') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">Email</label>
                <div class="col-md-9">
                    <input type="email" name="email" class="form-control" placeholder="Enter Business Email ....." />
                    <medium class="text-danger" >{{ $errors->first('email') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">Phone</label>
                <div class="col-md-9">
                    <input type="number" name="phone" class="form-control" placeholder="Enter Business Phone Number ....." />
                    <medium class="text-danger" >{{ $errors->first('phone') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">Total Employess</label>
                <div class="col-md-9">
                    <input type="number" name="total_employees" class="form-control" placeholder="Enter Total Employess Working On Your Company ....." />
                    <medium class="text-danger" >{{ $errors->first('total_employees') }}</medium>
                </div>
            </div>
            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">Opening Hour</label>
                <div class="col-md-9">
                    <input type="number" name="opening_hour" class="form-control" placeholder="Enter Total Opening Hour of Your Company ....." />
                    <medium class="text-danger" >{{ $errors->first('opening_hour') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">Established Date</label>
                <div class="col-md-9">
                    <input type="date" name="established" class="form-control" placeholder="Enter Business Established Date ....." />
                    <medium class="text-danger" >{{ $errors->first('established') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">Maximum Customer Handel</label>
                <div class="col-md-9">
                    <input type="text" name="max_customer_handle" class="form-control" placeholder="Enter Maximum Customer Handel ....." />
                    <medium class="text-danger" >{{ $errors->first('max_customer_handle') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">Website Link</label>
                <div class="col-md-9">
                    <input type="text" name="website_link" class="form-control" placeholder="Enter Website Link ....." />
                    <medium class="text-danger" >{{ $errors->first('website_link') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">Features</label>
                <div class="col-md-9">
                    <input type="text" name="feature" class="form-control" placeholder="Enter Features ....." />
                    <medium class="text-danger" >{{ $errors->first('feature') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">ABN-ACN</label>
                <div class="col-md-9">
                    <input type="text" name="abn_acn" class="form-control" placeholder="Enter ABN-ACN ....." />
                    <medium class="text-danger" >{{ $errors->first('abn_acn') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">About Business</label>
                <div class="col-md-9">
                    <textarea class="form-control ckeditor" id="editor1" name="about_business" rows="20">{{ old('about_business') }}</textarea>
                    <medium class="text-danger" >{{ $errors->first('about_business') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">Street Address</label>
                <div class="col-md-9">
                    <input type="text" name="street_address" class="form-control" placeholder="Enter Street Address ....." />
                    <medium class="text-danger" >{{ $errors->first('street_address') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">Suburb</label>
                <div class="col-md-9">
                    <input type="text" name="suburb" class="form-control" placeholder="Enter Suburb....." />
                    <medium class="text-danger" >{{ $errors->first('suburb') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">Post-Code</label>
                <div class="col-md-9">
                    <input type="text" name="postcode" class="form-control" placeholder="Enter Post-Code....." />
                    <medium class="text-danger" >{{ $errors->first('postcode') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">State</label>
                <div class="col-md-9">
                    <input type="text" name="state" class="form-control" placeholder="Enter State....." />
                    <medium class="text-danger" >{{ $errors->first('state') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">Country</label>
                <div class="col-md-9">
                    <input type="text" name="country" class="form-control" placeholder="Enter Contry....." />
                    <medium class="text-danger" >{{ $errors->first('country') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">Address</label>
                <div class="col-md-9">
                    <input type="text" name="address_string" class="form-control" placeholder="Enter String Address....." />
                    <medium class="text-danger" >{{ $errors->first('address_string') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">Latitude</label>
                <div class="col-md-9">
                    <input type="text" name="lat" class="form-control" placeholder="Enter String Latitude....." />
                    <medium class="text-danger" >{{ $errors->first('lat') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">Longitude</label>
                <div class="col-md-9">
                    <input type="text" name="lon" class="form-control" placeholder="Enter String Longitude....." />
                    <medium class="text-danger" >{{ $errors->first('lon') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">Operating Radious</label>
                <div class="col-md-9">
                    <input type="text" name="operating_radius" class="form-control" placeholder="Enter String Operating Radious....." />
                    <medium class="text-danger" >{{ $errors->first('operating_radius') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">Instagram</label>
                <div class="col-md-9">
                    <input type="text" name="instagram" class="form-control" placeholder="Enter String Instagram....." />
                    <medium class="text-danger" >{{ $errors->first('instagram') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">Twitter</label>
                <div class="col-md-9">
                    <input type="text" name="twitter" class="form-control" placeholder="Enter String Twitter....." />
                    <medium class="text-danger" >{{ $errors->first('twitter') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">Facebook</label>
                <div class="col-md-9">
                    <input type="text" name="facebook" class="form-control" placeholder="Enter String Facebook....." />
                    <medium class="text-danger" >{{ $errors->first('facebook') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">Business Profile Image</label>
                <div class="col-md-9">
                    <input type="file" name="business_profile_image" class="form-control" />
                    <medium class="text-danger" >{{ $errors->first('business_profile_image') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">Business Cover Image</label>
                <div class="col-md-9">
                    <input type="file" name="business_cover_image" class="form-control"/>
                    <medium class="text-danger" >{{ $errors->first('business_cover_image') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-15">
                <label class="col-form-label col-md-3">Default Music</label>
                <div class="col-md-9">
                    <input type="file" name="default_music" class="form-control" />
                    <medium class="text-danger" >{{ $errors->first('default_music') }}</medium>
                </div>
            </div>

            <div class="form-group row m-b-0">
                <label class="col-md-4 col-sm-4 col-form-label">&nbsp;</label>
                <div class="col-md-8 col-sm-8">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
    <!-- end panel-body -->

</div>
<!-- end panel -->

@endsection

@section('page_specific_scripts')
    <script src="{{ asset('assets/admin/plugins/ckeditor/ckeditor.js') }}"></script>
@endsection
