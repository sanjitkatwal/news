@extends('backend.layouts.app')
@section('page_title')
    CMS-Business-Manager
@endsection

@section('breadcomes')
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashbaord</a></li>
        <li class="breadcrumb-item active">Business View</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Business Edit <small>header small text goes here...</small></h1>
    <!-- end page-header -->
@endsection

@section('content')
    <!-- begin panel -->
    <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
        <!-- begin panel-heading -->
        <div class="panel-heading ui-sortable-handle">
            <h4 class="panel-title">Form Controls For Business Edit</h4>
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
        </div>
        <!-- end panel-heading -->
        <!-- begin panel-body -->
        <div class="panel-body">
                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Business Name</label>
                    <div class="col-md-9">
                        <input type="text" name="business_name" class="form-control" value="{{ $data['row']->business_name }}" placeholder="Enter Business Name ....." />
                    </div>
                </div>

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Business Type</label>
                    <div class="col-md-9">
                        <input type="text" name="business_type" class="form-control" value="{{ $data['row']->business_type }}" placeholder="Enter Business Type ....." />
                        <medium class="text-danger" >{{ $errors->first('business_type') }}</medium>
                    </div>
                </div>

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Email</label>
                    <div class="col-md-9">
                        <input type="email" name="email" class="form-control" value="{{ $data['row']->email }}" placeholder="Enter Business Email ....." />
                        <medium class="text-danger" >{{ $errors->first('email') }}</medium>
                    </div>
                </div>

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Phone</label>
                    <div class="col-md-9">
                        <input type="number" name="phone" class="form-control" value="{{ $data['row']->phone }}" placeholder="Enter Business Phone Number ....." />
                        <medium class="text-danger" >{{ $errors->first('phone') }}</medium>
                    </div>
                </div>

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Total Employess</label>
                    <div class="col-md-9">
                        <input type="number" name="total_employees" class="form-control" value="{{ $data['row']->total_employees }}" placeholder="Enter Total Employess Working On Your Company ....." />
                        <medium class="text-danger" >{{ $errors->first('total_employees') }}</medium>
                    </div>
                </div>
                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Opening Hour</label>
                    <div class="col-md-9">
                        <input type="number" name="opening_hour" class="form-control" value="{{ $data['row']->opening_hour }}" placeholder="Enter Total Opening Hour of Your Company ....." />
                        <medium class="text-danger" >{{ $errors->first('opening_hour') }}</medium>
                    </div>
                </div>

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Established Date</label>
                    <div class="col-md-9">
                        <input type="text" name="established" class="form-control" value="{{ $data['row']->established }}" placeholder="Enter Business Established Date ....." />
                        <medium class="text-danger" >{{ $errors->first('established') }}</medium>
                    </div>
                </div>

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Maximum Customer Handel</label>
                    <div class="col-md-9">
                        <input type="text" name="max_customer_handle" class="form-control" value="{{ $data['row']->max_customer_handle }}" placeholder="Enter Maximum Customer Handel ....." />
                        <medium class="text-danger" >{{ $errors->first('max_customer_handle') }}</medium>
                    </div>
                </div>

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Website Link</label>
                    <div class="col-md-9">
                        <input type="text" name="website_link" class="form-control" value="{{ $data['row']->websie_link }}" placeholder="Enter Website Link ....." />
                        <medium class="text-danger" >{{ $errors->first('website_link') }}</medium>
                    </div>
                </div>

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Features</label>
                    <div class="col-md-9">
                        <input type="text" name="feature" class="form-control" value="{{ $data['row']->feature }}" placeholder="Enter Features ....." />
                        <medium class="text-danger" >{{ $errors->first('feature') }}</medium>
                    </div>
                </div>

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">ABN-ACN</label>
                    <div class="col-md-9">
                        <input type="text" name="abn_acn" class="form-control" value="{{ $data['row']->abn_acn }}" placeholder="Enter ABN-ACN ....." />
                        <medium class="text-danger" >{{ $errors->first('abn_acn') }}</medium>
                    </div>
                </div>

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">About Business</label>
                    <div class="col-md-9">
                        <textarea class="form-control ckeditor" id="editor1" name="about_business" rows="20">{{ old('about_business') }} {{ $data['row']->about_business }}</textarea>
                        <medium class="text-danger" >{{ $errors->first('about_business') }}</medium>
                    </div>
                </div>

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Street Address</label>
                    <div class="col-md-9">
                        <input type="text" name="street_address" class="form-control" value="{{ $data['row']->street_address }}" placeholder="Enter Street Address ....." />
                        <medium class="text-danger" >{{ $errors->first('street_address') }}</medium>
                    </div>
                </div>

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Suburb</label>
                    <div class="col-md-9">
                        <input type="text" name="suburb" class="form-control" value="{{ $data['row']->suburb }}" placeholder="Enter Suburb....." />
                        <medium class="text-danger" >{{ $errors->first('suburb') }}</medium>
                    </div>
                </div>

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Post-Code</label>
                    <div class="col-md-9">
                        <input type="text" name="postcode" class="form-control" value="{{ $data['row']->postcode }}" placeholder="Enter Post-Code....." />
                        <medium class="text-danger" >{{ $errors->first('postcode') }}</medium>
                    </div>
                </div>

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">State</label>
                    <div class="col-md-9">
                        <input type="text" name="state" class="form-control" value="{{ $data['row']->state }}" placeholder="Enter State....." />
                        <medium class="text-danger" >{{ $errors->first('state') }}</medium>
                    </div>
                </div>

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Country</label>
                    <div class="col-md-9">
                        <input type="text" name="country" class="form-control" value="{{ $data['row']->country }}" placeholder="Enter Contry....." />
                        <medium class="text-danger" >{{ $errors->first('country') }}</medium>
                    </div>
                </div>

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Address</label>
                    <div class="col-md-9">
                        <input type="text" name="address_string" class="form-control" value="{{ $data['row']->address_string }}" placeholder="Enter String Address....." />
                        <medium class="text-danger" >{{ $errors->first('address_string') }}</medium>
                    </div>
                </div>

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Latitude</label>
                    <div class="col-md-9">
                        <input type="text" name="lat" class="form-control" value="{{ $data['row']->lat }}" placeholder="Enter String Latitude....." />
                        <medium class="text-danger" >{{ $errors->first('lat') }}</medium>
                    </div>
                </div>

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Longitude</label>
                    <div class="col-md-9">
                        <input type="text" name="lon" class="form-control" value="{{ $data['row']->lon }}" placeholder="Enter String Longitude....." />
                        <medium class="text-danger" >{{ $errors->first('lon') }}</medium>
                    </div>
                </div>

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Operating Radious</label>
                    <div class="col-md-9">
                        <input type="text" name="operating_radius" class="form-control" value="{{ $data['row']->operating_radius }}" placeholder="Enter String Operating Radious....." />
                        <medium class="text-danger" >{{ $errors->first('operating_radius') }}</medium>
                    </div>
                </div>

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Instagram</label>
                    <div class="col-md-9">
                        <input type="text" name="instagram" class="form-control" value="{{ $data['row']->instagram }}" placeholder="Enter String Instagram....." />
                        <medium class="text-danger" >{{ $errors->first('instagram') }}</medium>
                    </div>
                </div>

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Twitter</label>
                    <div class="col-md-9">
                        <input type="text" name="twitter" class="form-control" value="{{ $data['row']->twitter }}" placeholder="Enter String Twitter....." />
                        <medium class="text-danger" >{{ $errors->first('twitter') }}</medium>
                    </div>
                </div>

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Facebook</label>
                    <div class="col-md-9">
                        <input type="text" name="facebook" class="form-control" value="{{ $data['row']->facebook }}" placeholder="Enter String Facebook....." />
                        <medium class="text-danger" >{{ $errors->first('facebook') }}</medium>
                    </div>
                </div>

                @if (isset($data['row']))
                    @if($data['row']->business_profile_image)
                        <div class="form-group row m-b-15">
                            <label class="col-form-label col-md-3">Business Profile Image</label>
                            <div class="col-md-9">
                                <img src="{{ asset('storage/business_profile_images/'.$data['row']->business_profile_image) }}" alt="No Image Available" style="width: 60px; height: 70px;">
                                <medium class="text-danger" >{{ $errors->first('business_profile_image') }}</medium>
                            </div>
                        </div>
                    @else
                        <div class="form-group row m-b-15">
                            <label class="col-form-label col-md-3">Business Profile Image</label>
                            <div class="col-md-9">
                                <p>No Image Found!</p>
                            </div>
                        </div>
                    @endif

                @endif
                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Business Profile Image</label>
                    <div class="col-md-9">
                        <input type="file" name="business_profile_image" class="form-control"/>
                        <medium class="text-danger" >{{ $errors->first('business_profile_image') }}</medium>
                    </div>
                </div>

                @if (isset($data['row']))
                    @if($data['row']->business_cover_image)
                        <div class="form-group row m-b-15">
                            <label class="col-form-label col-md-3">Business Cover Image</label>
                            <div class="col-md-9">
                                <img src="{{ asset('storage/business_cover_images/'.$data['row']->business_cover_image) }}" alt="No Image Available" style="width: 60px; height: 70px;">
                                <medium class="text-danger" >{{ $errors->first('business_cover_image') }}</medium>
                            </div>
                        </div>
                    @else
                        <div class="form-group row m-b-15">
                            <label class="col-form-label col-md-3">Business Cover Image</label>
                            <div class="col-md-9">
                                <p>No Image Found!</p>
                            </div>
                        </div>
                    @endif

                @endif

                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Business Cover Image</label>
                    <div class="col-md-9">
                        <input type="file" name="business_cover_image" class="form-control" value="{{ $data['row'] }}"/>
                        <medium class="text-danger" >{{ $errors->first('business_cover_image') }}</medium>
                    </div>
                </div>


                @if (isset($data['row']))
                    @if($data['row']->default_music)
                        <div class="form-group row m-b-15">
                            <label class="col-form-label col-md-3">Default Music</label>
                            <div class="col-md-9">
                                <img src="{{ asset('storage/business_default_music/'.$data['row']->default_music) }}" alt="No Music Available">
                                <medium class="text-danger" >{{ $errors->first('default_music') }}</medium>
                            </div>
                        </div>
                    @else
                        <div class="form-group row m-b-15">
                            <label class="col-form-label col-md-3">Default Music</label>
                            <div class="col-md-9">
                                <p>No Music Found!</p>
                            </div>
                        </div>
                    @endif

                @endif
                <div class="form-group row m-b-15">
                    <label class="col-form-label col-md-3">Default Music</label>
                    <div class="col-md-9">
                        <input type="file" name="default_music" class="form-control"/>
                        <medium class="text-danger" >{{ $errors->first('default_music') }}</medium>
                    </div>
                </div>
        </div>
        <!-- end panel-body -->

    </div>
    <!-- end panel -->

@endsection
@section('page_specific_scripts')
    <script src="{{ asset('assets/admin/plugins/ckeditor/ckeditor.js') }}"></script>
@endsection
