@extends('backend.layouts.app')
@section('page_title')
    CMS-Post-Manager
@endsection

@section('breadcomes')
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashbaord</a></li>
        <li class="breadcrumb-item active">Post Create</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Post Create <small>header small text goes here...</small></h1>
    <!-- end page-header -->
@endsection

@section('content')
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
            <!-- begin panel-heading -->
            <div class="panel-heading ui-sortable-handle">
                <h4 class="panel-title">Form Controls</h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <!-- end panel-heading -->
            <!-- begin panel-body -->
            <div class="panel-body">
                <form action="{{ route('admin.post-galleries.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row m-b-10">
                            <label class="col-form-label col-md-3">Event Name</label>
                        <div class="col-md-9">
                            <select name="event_id" class="form-control mb-3">
                            @if(count($data['event']) > 0)
                                <option value="" select>Select Event Name</option>
                                @foreach($data['event'] as $event)
                                    <option value="{{ $event->id }}">{!! $event->event_title !!}</option>
                                @endforeach

                                @else
                                    <option>No Post Is Available.</option>
                                @endif
                            </select>
                            <medium class="text-danger" >{{ $errors->first('event_title') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-10">
                        <label class="col-form-label col-md-3">Business Offer</label>
                        <div class="col-md-9">
                            <select name="business_offer_id" class="form-control mb-3">
                                @if(count($data['business_offer']) > 0)
                                    <option value="" select>Select Discount Offer (%)</option>
                                    @foreach($data['business_offer'] as $business_offer)
                                        <option value="{{ $business_offer->id }}">{!! $business_offer->discount_percentage !!}</option>
                                    @endforeach

                                @else
                                    <option>No Post Is Available.</option>
                                @endif
                            </select>
                            <medium class="text-danger" >{{ $errors->first('discount_percentage') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-10">
                        <label class="col-form-label col-md-3">Business Portfolio Name</label>
                        <div class="col-md-9">
                            <select name="business_offer_id" class="form-control mb-3">
                                @if(count($data['business_offer']) > 0)
                                    <option value="" select>Select Business Portfolio Name</option>
                                    @foreach($data['business_portfolio'] as $business_portfolio)
                                        <option value="{{ $business_portfolio->id }}">{!! $business_portfolio->name !!}</option>
                                    @endforeach

                                @else
                                    <option>No Post Is Available.</option>
                                @endif
                            </select>
                            <medium class="text-danger" >{{ $errors->first('name') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-10">
                        <label class="col-form-label col-md-3">Visibility Type</label>
                        <div class="col-md-9">
                            <select name="visibility_type" class="form-control mb-3">
                                <option value="" select>Select Visibility Type</option>
                                <option value="Public">Public</option>
                                <option value="Private">Private</option>
                            </select>
                            <medium class="text-danger" >{{ $errors->first('discount_percentage') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Live Key</label>
                        <div class="col-md-9">
                            <input type="text" name="live_key" class="form-control" placeholder="Enter Live Key ....." />
                            <medium class="text-danger" >{{ $errors->first('live_key') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Country</label>
                        <div class="col-md-9">
                            <input type="text" name="source_country" class="form-control" placeholder="Enter Country Name ....." />
                            <medium class="text-danger" >{{ $errors->first('source_country') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Message</label>
                        <div class="col-md-9">
                            <textarea class="form-control ckeditor" id="editor1" name="message" rows="20"></textarea>
                            <medium class="text-danger" >{{ $errors->first('message') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-0">
                        <label class="col-md-4 col-sm-4 col-form-label">&nbsp;</label>
                        <div class="col-md-8 col-sm-8">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>


                </form>
            </div>
            <!-- end panel-body -->

        </div>
        <!-- end panel -->

@endsection
@section('page_specific_scripts')
    <script src="{{ asset('assets/admin/plugins/ckeditor/ckeditor.js') }}"></script>
@endsection
