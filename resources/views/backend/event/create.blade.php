@extends('backend.layouts.app')
@section('page_title')
    CMS-Event-Manager
@endsection

@section('breadcomes')
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashbaord</a></li>
        <li class="breadcrumb-item active">Event Manager Create Form</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Event Manager Create Form <medium>header medium text goes here...</medium></h1>
    <!-- end page-header -->
@endsection

@section('content')
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
            <!-- begin panel-heading -->
            <div class="panel-heading ui-sortable-handle">
                <h4 class="panel-title">Event Manager Create Form</h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <!-- end panel-heading -->
            <!-- begin panel-body -->

                <div class="panel-body">
                <form action="{{ route('admin.event-manager.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Event Title</label>
                        <div class="col-md-9">
                            <input type="text" name="event_title" value="{{ old('event_title') }}" class="form-control" placeholder="Enter Event Title ....." />
                            <medium class="text-danger" >{{ $errors->first('event_title') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Address</label>
                        <div class="col-md-9">
                            <input type="text" value="{{ old('address_string') }}" name="address_string" class="form-control" placeholder="Enter Address ....." />
                            <medium class="text-danger" >{{ $errors->first('address_string') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Street</label>
                        <div class="col-md-9">
                            <input type="text" name="street" class="form-control" placeholder="Enter Street ....." />
                            <medium class="text-danger" >{{ $errors->first('street') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Sub Urban</label>
                        <div class="col-md-9">
                            <input type="text" name="suburb" class="form-control" placeholder="Enter Sub Urban....." />
                            <medium class="text-danger" >{{ $errors->first('suburb') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Post Code</label>
                        <div class="col-md-9">
                            <input type="text" name="postcode" class="form-control" placeholder="Enter Post Code ....."/>
                            <medium class="text-danger" >{{ $errors->first('postcode') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">State</label>
                        <div class="col-md-9">
                            <input type="text" name="state" class="form-control" placeholder="Enter State....." />
                            <medium class="text-danger" >{{ $errors->first('state') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Country</label>
                        <div class="col-md-9">
                            <input type="text" name="country" class="form-control" placeholder="Enter Contry....." />
                            <medium class="text-danger" >{{ $errors->first('country') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Event Type</label>
                        <div class="col-md-9">
                            <input type="text" name="event_type" class="form-control" placeholder="Enter Event Type....." />
                            <medium class="text-danger" >{{ $errors->first('event_type') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Event Start Date</label>
                        <div class="col-md-9">
                            <input type="date" name="event_start_date" class="form-control" placeholder="Enter Event Start Date....." />
                            <medium class="text-danger" >{{ $errors->first('event_start_date') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Event End Date</label>
                        <div class="col-md-9">
                            <input type="date" name="event_end_date" class="form-control" placeholder="Enter Event Start Time....." />
                            <medium class="text-danger" >{{ $errors->first('event_end_date') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Event Start Time</label>
                        <div class="col-md-9">
                            <input type="time" name="event_start_time" class="form-control" placeholder="Enter Event Start Time....." />
                            <medium class="text-danger" >{{ $errors->first('event_start_time') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Description</label>
                        <div class="col-md-9">
                            <textarea class="form-control ckeditor" id="editor1" name="description" rows="20"></textarea>
                            <medium class="text-danger" >{{ $errors->first('description') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Latitude</label>
                        <div class="col-md-9">
                            <input type="text" name="lat" class="form-control" placeholder="Enter String Latitude....." />
                            <medium class="text-danger" >{{ $errors->first('lat') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Longitude</label>
                        <div class="col-md-9">
                            <input type="text" name="lon" class="form-control" placeholder="Enter String Longitude....." />
                            <medium class="text-danger" >{{ $errors->first('lon') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-0">
                        <label class="col-md-4 col-sm-4 col-form-label">&nbsp;</label>
                        <div class="col-md-8 col-sm-8">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>


                </form>
            </div>
            <!-- end panel-body -->

        </div>
        <!-- end panel -->

@endsection

@section('page_specific_scripts')
    <script src="{{ asset('assets/admin/plugins/ckeditor/ckeditor.js') }}"></script>
@endsection
