@extends('backend.layouts.app')
@section('page_title')
    CMS-Event-Manager
@endsection

@section('breadcomes')
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashbaord</a></li>
        <li class="breadcrumb-item active">Event Manager Edit Form</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Event Manager Edit Form <small>header small text goes here...</small></h1>
    <!-- end page-header -->
@endsection

@section('content')
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
            <!-- begin panel-heading -->
            <div class="panel-heading ui-sortable-handle">
                <h4 class="panel-title">Event Manager Edit Form</h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <!-- end panel-heading -->
            <!-- begin panel-body -->
            <div class="panel-body">
                <form action="{{ route('admin.event-manager.update', $data['row']->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="portfolio_id" value="{{ $data['row']->id }}">

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Event Title</label>
                        <div class="col-md-9">
                            <input type="text" name="event_title" value="{{$data['row']->event_title}}" class="form-control" placeholder="Enter Event Title ....." />
                            <small class="text-danger" >{{ $errors->first('event_title') }}</small>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Address</label>
                        <div class="col-md-9">
                            <input type="text" name="address_string" value="{{ $data['row']->address_string }}" class="form-control" placeholder="Enter Address ....." />
                            <small class="text-danger" >{{ $errors->first('address_string') }}</small>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Street</label>
                        <div class="col-md-9">
                            <input type="text" name="street" value="{{ $data['row']->street }}" class="form-control" placeholder="Enter Street ....." />
                            <small class="text-danger" >{{ $errors->first('street') }}</small>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Sub Urban</label>
                        <div class="col-md-9">
                            <input type="text" name="suburb" value="{{ $data['row']->suburb }}" class="form-control" placeholder="Enter Sub Urban....." />
                            <small class="text-danger" >{{ $errors->first('suburb') }}</small>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Post Code</label>
                        <div class="col-md-9">
                            <input type="text" name="postcode" value="{{ $data['row']->postcode }}" class="form-control" placeholder="Enter Post Code ....." />
                            <small class="text-danger" >{{ $errors->first('postcode') }}</small>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">State</label>
                        <div class="col-md-9">
                            <input type="text" name="state" value="{{ $data['row']->state }}" class="form-control" placeholder="Enter State....." />
                            <small class="text-danger" >{{ $errors->first('state') }}</small>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Country</label>
                        <div class="col-md-9">
                            <input type="text" name="country" value="{{ $data['row']->country }}" class="form-control" placeholder="Enter Contry....." />
                            <small class="text-danger" >{{ $errors->first('country') }}</small>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Event Type</label>
                        <div class="col-md-9">
                            <input type="text" name="event_type" value="{{ $data['row']->event_type }}" class="form-control" placeholder="Enter Event Type....." />
                            <small class="text-danger" >{{ $errors->first('event_type') }}</small>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Event Start Date</label>
                        <div class="col-md-9">
                            <input type="date" name="event_start_date" value="{{ $data['row']->event_start_date }}" class="form-control" placeholder="Enter Event Start Date....." />
                            <small class="text-danger" >{{ $errors->first('event_start_date') }}</small>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Event End Date</label>
                        <div class="col-md-9">
                            <input type="date" name="event_end_date" value="{{ $data['row']->event_end_date }}" class="form-control" placeholder="Enter Event Start Time....." />
                            <small class="text-danger" >{{ $errors->first('event_end_date') }}</small>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Event Start Time</label>
                        <div class="col-md-9">
                            <input type="time" name="event_start_time" value="{{ $data['row']->event_start_time }}" class="form-control" placeholder="Enter Event Start Time....." />
                            <small class="text-danger" >{{ $errors->first('event_start_time') }}</small>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Description</label>
                        <div class="col-md-9">
                            <textarea class="form-control ckeditor" id="editor1" name="description" rows="20">{{ $data['row']->description }}</textarea>
                            <small class="text-danger" >{{ $errors->first('description') }}</small>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Latitude</label>
                        <div class="col-md-9">
                            <input type="text" name="lat" value="{{ $data['row']->lat }}" class="form-control" placeholder="Enter String Latitude....." />
                            <small class="text-danger" >{{ $errors->first('lat') }}</small>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Longitude</label>
                        <div class="col-md-9">
                            <input type="text" name="lon" value="{{ $data['row']->lon }}" class="form-control" placeholder="Enter String Longitude....." />
                            <small class="text-danger" >{{ $errors->first('lon') }}</small>
                        </div>
                    </div>

                    <div class="form-group row m-b-0">
                        <label class="col-md-4 col-sm-4 col-form-label">&nbsp;</label>
                        <div class="col-md-8 col-sm-8">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

@endsection

@section('page_specific_scripts')
    <script src="{{ asset('assets/admin/plugins/ckeditor/ckeditor.js') }}"></script>
@endsection
