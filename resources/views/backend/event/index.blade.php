@extends('backend.layouts.app')
@section('page_title')
    CMS-Event-Manager
@endsection

@section('page_specific_css')
    <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
    <link href="{{ asset('assets/admin/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/admin/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}" rel="stylesheet" />
    <!-- ================== END PAGE LEVEL STYLE ================== -->
    @endsection

@section('breadcomes')
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashbaord</a></li>
        <li class="breadcrumb-item active">Event List</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Event List <small>header small text goes here...</small></h1>
    <!-- end page-header -->
@endsection

@section('content')
    @include('backend.layouts.common.message')
    <div class="panel panel-inverse">
        <!-- begin panel-body -->
        <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12">
                        <table id="data-table-responsive" class="table table-striped table-bordered table-td-valign-middle dataTable no-footer dtr-inline" role="grid" aria-describedby="data-table-responsive_info" style="width: 100%;">
                            <thead>
                            <tr role="row"><th width="1%" class="sorting_asc" tabindex="0" aria-controls="data-table-responsive" rowspan="1" colspan="1" style="width: 0px;" aria-sort="ascending" aria-label=": activate to sort column descending"></th>
                                <th class="text-nowrap sorting" tabindex="0" aria-controls="data-table-responsive" rowspan="1" colspan="1" style="width: 118px;" aria-label="Rendering engine: activate to sort column ascending">Created By</th>
                                <th class="text-nowrap sorting" tabindex="0" aria-controls="data-table-responsive" rowspan="1" colspan="1" style="width: 118px;" aria-label="Rendering engine: activate to sort column ascending">Event Title</th>
                                <th class="text-nowrap sorting" tabindex="0" aria-controls="data-table-responsive" rowspan="1" colspan="1" style="width: 100px;" aria-label="Engine version: activate to sort column ascending">Address</th>
                                <th class="text-nowrap sorting" tabindex="0" aria-controls="data-table-responsive" rowspan="1" colspan="1" style="width: 118px;" aria-label="Rendering engine: activate to sort column ascending">State</th>
                                <th class="text-nowrap sorting" tabindex="0" aria-controls="data-table-responsive" rowspan="1" colspan="1" style="width: 152px;" aria-label="Browser: activate to sort column ascending">Country</th>
                                <th class="text-nowrap sorting" tabindex="0" aria-controls="data-table-responsive" rowspan="1" colspan="1" style="width: 135px;" aria-label="Platform(s): activate to sort column ascending">Event Type</th>
                                <th class="text-nowrap sorting" tabindex="0" aria-controls="data-table-responsive" rowspan="1" colspan="1" style="width: 70px;" aria-label="CSS grade: activate to sort column ascending">Operation</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data['rows'] as $row)
                                <tr class="gradeX odd" role="row">
                                    <td width="1%" class="f-s-600 text-inverse sorting_1" tabindex="0">{{ $no++ }}</td>
                                    <td>{!! $row->creator_id !!}</td>
                                    <td>{!! $row->event_title !!}</td>
                                    <td>{!! $row->address_string !!}</td>
                                    <td>{{ $row->state }}</td>
                                    <td>{!! $row->country !!}</td>
                                    <td>{!! $row->event_type !!}</td>
                                    <td class="with-btn" nowrap="">
                                        <a href="{{route('admin.event-manager.edit', ['id'=>$row->id])}}" class="btn btn-sm btn-primary width-60 m-r-2">Edit</a>
                                        <a href="{{route('admin.event-manager.delete', ['id'=>$row->id])}}" class="btn btn-sm btn-danger width-60" onclick="return confirm('Are you sure want to delete this item?')">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                           </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- end panel-body -->
    </div>
<!-- end panel -->

@endsection


@section('page_specific_scripts')
    <!-- ================== BEGIN PAGE LEVEL JS ================== -->
    <script src="{{ asset('assets/admin/plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/demo/table-manage-responsive.demo.js') }}"></script>
    <!-- ================== END PAGE LEVEL JS ================== -->

    <script type="text/javascript">
        $('#my_table').DataTable( {
            responsive: false,
            paging     : false,
            searching   : false,
        } );
    </script>
    @endsection


