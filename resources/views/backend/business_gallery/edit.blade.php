@extends('backend.layouts.app')
@section('page_title')
    CMS-Business-Gallery-Manager
@endsection

@section('breadcomes')
    <!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashbaord</a></li>
        <li class="breadcrumb-item active">Business Gallery Edit</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Business Gallery Edit <small>header small text goes here...</small></h1>
    <!-- end page-header -->
@endsection

@section('content')
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
            <!-- begin panel-heading -->
            <div class="panel-heading ui-sortable-handle">
                <h4 class="panel-title">Edit Control Form</h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <!-- end panel-heading -->
            <!-- begin panel-body -->
            <div class="panel-body">
                <form action="{{ route('admin.business-gallery.update', $data['row']->id) }}" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group row m-b-10">
                        <label class="col-form-label col-md-3">Business Name</label>
                        <div class="col-md-9">
                            <select name="business_id" class="form-control mb-3">
                                @if(count($data['business']) > 0)
                                    <option value="" select>Select Your Business Name</option>
                                    @foreach($data['business'] as $business)
                                        <option value="{{ $business->id }}"
                                            {{isset($data['row']) && $data['row']->business_id == $business->id?'selected':''}}>
                                            {!! $business->business_name !!}</option>
                                    @endforeach

                                @else
                                    <option>No Business Name Is Available.</option>
                                @endif
                            </select>
                            <medium class="text-danger" >{{ $errors->first('business_id') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-10">
                        <label class="col-form-label col-md-3">Media Type</label>
                        <div class="col-md-9">
                            <select name="media_type" class="form-control mb-3">
                                <option value="" selected>Select Media Type</option>
                                <option value="image" {{ isset($data['row'])&& $data['row']->media_type == 'image'?'selected':'' }}>Image</option>
                                <option value="video" {{ isset($data['row'])&& $data['row']->media_type == 'video'?'selected':'' }}>Vidoe</option>
                                <option value="audio" {{ isset($data['row'])&& $data['row']->media_type == 'audio'?'selected':'' }}>Audio</option>
                            </select>
                            <medium class="text-danger" >{{ $errors->first('media_type') }}</medium>
                        </div>
                    </div>

                    @if (isset($data['row']))
                        @if($data['row']->filepath)
                            <div class="form-group row m-b-15">
                                <label class="col-form-label col-md-3">Your Old File</label>
                                <div class="col-md-9">
                                    <img src="{{ asset('storage/business_gallery_images/'.$data['row']->filepath) }}" alt="No Image Available" style="width: 60px; height: 70px;">
                                    <medium class="text-danger" >{{ $errors->first('filepath') }}</medium>
                                </div>
                            </div>
                        @else
                            <div class="form-group row m-b-15">
                                <label class="col-form-label col-md-3">Select File</label>
                                <div class="col-md-9">
                                    <p>No Image Found!</p>
                                </div>
                            </div>
                        @endif

                    @endif
                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Select New File</label>
                        <div class="col-md-9">
                            <input type="file" name="filepath" class="form-control"/>
                            <medium class="text-danger" >{{ $errors->first('filepath') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Caption</label>
                        <div class="col-md-9">
                            <input type="text" name="caption" value="{{ $data['row']->caption }}" class="form-control" placeholder="Enter Caption ....." />
                            <medium class="text-danger" >{{ $errors->first('caption') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Description</label>
                        <div class="col-md-9">
                            <textarea class="form-control ckeditor" id="editor1" name="description" rows="20">
                                {{ $data['row']->description }}
                            </textarea>
                            <medium class="text-danger" >{{ $errors->first('description') }}</medium>
                        </div>
                    </div>

                    <div class="form-group row m-b-0">
                        <label class="col-md-4 col-sm-4 col-form-label">&nbsp;</label>
                        <div class="col-md-8 col-sm-8">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>


                </form>
            </div>
            <!-- end panel-body -->

        </div>
        <!-- end panel -->

@endsection

@section('page_specific_scripts')
    <script src="{{ asset('assets/admin/plugins/ckeditor/ckeditor.js') }}"></script>
@endsection
