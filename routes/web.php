<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/*
 *
 * Backend route start from here
 *
 */

Route::middleware(['middleware' => 'auth'])->group(function () {

Route::get('admin/dashboard',       ['as' => 'admin.dashboard', 'uses' => 'Backend\DashboardController@index']);
Route::get('admin/report',          ['as' => 'admin.report', 'uses' => 'Backend\DashboardController@report']);
Route::get('admin/error/{code}',    ['as' => 'admin.error',     'uses' => 'Backend\DashboardController@error']);


Route::get('admin/user-manager/list',                  ['as' => 'admin.user-manager.list',              'uses' => 'Backend\UserManagerController@index']);
Route::get('admin/user-manager/deleted-list',          ['as' => 'admin.user-manager.deleted-list',      'uses' => 'Backend\UserManagerController@deletedList']);
Route::get('admin/user-manager/create',                ['as' => 'admin.user-manager.create',            'uses' => 'Backend\UserManagerController@userCreate']);
Route::post('admin/user-manager/store',                ['as' => 'admin.user-manager.store',             'uses' => 'Backend\UserManagerController@userStore']);
Route::get('admin/user-manager/{id}/edit',             ['as' => 'admin.user-manager.user-edit',         'uses' => 'Backend\UserManagerController@userEdit']);
Route::post('admin/user-manager/{id}/user-update',     ['as' => 'admin.user-manager.user-update',      'uses' => 'Backend\UserManagerController@userUpdate']);
Route::get('admin/user-manager/{id}/delete',           ['as' => 'admin.user-manager.delete',            'uses' => 'Backend\UserManagerController@destroy']);

Route::get('admin/user-profile/create',                ['as' => 'admin.user-profile.create',            'uses' => 'Backend\UserManagerController@userProfilecreate']);
Route::post('admin/user-profile/store',                ['as' => 'admin.user-profile.store',             'uses' => 'Backend\UserManagerController@userProfileStore']);
Route::get('admin/user-profile/{id}/delete',           ['as' => 'admin.user-profile.delete',            'uses' => 'Backend\UserManagerController@userDelete']);
Route::get('admin/user-profile/{id}/permanent-delete', ['as' => 'admin.user-profile.permanent-delete',  'uses' => 'Backend\UserManagerController@permanentDestroy']);
Route::get('admin/user-profile/{id}/user-recover',     ['as' => 'admin.user-profile.user-recover',      'uses' => 'Backend\UserManagerController@userRecover']);
Route::get('admin/user-profile/{id}/change-status',    ['as' => 'admin.user-profile.change-status',     'uses' => 'Backend\UserManagerController@changeStatusForm']);
Route::post('admin/user-profile/{id}/update-status',    ['as' => 'admin.user-profile.update-status',     'uses' => 'Backend\UserManagerController@updateStatus']);


Route::get('admin/user-music/list',                    ['as' => 'admin.user-music.list',        'uses' => 'Backend\UserMusicController@index']);
Route::get('admin/user-music/create',                  ['as' => 'admin.user-music.create',          'uses' => 'Backend\UserMusicController@create']);
Route::post('admin/user-music/store',                  ['as' => 'admin.user-music.store',           'uses' => 'Backend\UserMusicController@store']);
Route::get('admin/user-music/{id}/edit',               ['as' => 'admin.user-music.edit',            'uses' => 'Backend\UserMusicController@edit']);
Route::post('admin/user-music/{id}/update',            ['as' => 'admin.user-music.update',          'uses' => 'Backend\UserMusicController@update']);
Route::get('admin/user-music/{id}/delete',             ['as' => 'admin.user-music.delete',          'uses' => 'Backend\UserMusicController@destroy']);


Route::get('admin/business-manager/list',                  ['as' => 'admin.business-manager.list',        'uses' => 'Backend\BusinessController@index']);
Route::get('admin/business-manager/create',                ['as' => 'admin.business-manager.create',      'uses' => 'Backend\BusinessController@create']);
Route::post('admin/business-manager/store',                ['as' => 'admin.business-manager.store',       'uses' => 'Backend\BusinessController@store']);
Route::get('admin/business-manager/{id}/edit',             ['as' => 'admin.business-manager.edit',        'uses' => 'Backend\BusinessController@edit']);
Route::get('admin/business-manager/{id}/show',             ['as' => 'admin.business-manager.show',        'uses' => 'Backend\BusinessController@show']);
Route::post('admin/business-manager/{id}/update',          ['as' => 'admin.business-manager.update',      'uses' => 'Backend\BusinessController@update']);
Route::get('admin/business-manager/{id}/delete',           ['as' => 'admin.business-manager.delete',      'uses' => 'Backend\BusinessController@destroy']);

Route::get('admin/business-gallery/list',                  ['as' => 'admin.business-gallery.list',          'uses' => 'Backend\BusinessGalleryController@index']);
Route::get('admin/business-gallery/create',                ['as' => 'admin.business-gallery.create',        'uses' => 'Backend\BusinessGalleryController@create']);
Route::post('admin/business-gallery/store',                ['as' => 'admin.business-gallery.store',         'uses' => 'Backend\BusinessGalleryController@store']);
Route::get('admin/business-gallery/{id}/edit',             ['as' => 'admin.business-gallery.edit',          'uses' => 'Backend\BusinessGalleryController@edit']);
Route::post('admin/business-gallery/{id}/update',          ['as' => 'admin.business-gallery.update',        'uses' => 'Backend\BusinessGalleryController@update']);
Route::get('admin/business-gallery/{id}/delete',           ['as' => 'admin.business-gallery.delete',        'uses' => 'Backend\BusinessGalleryController@destroy']);

Route::get('admin/business-music/list',                    ['as' => 'admin.business-music.list',            'uses' => 'Backend\BusinessMusicController@index']);
Route::get('admin/business-music/create',                  ['as' => 'admin.business-music.create',          'uses' => 'Backend\BusinessMusicController@create']);
Route::post('admin/business-music/store',                  ['as' => 'admin.business-music.store',           'uses' => 'Backend\BusinessMusicController@store']);
Route::get('admin/business-music/{id}/edit',               ['as' => 'admin.business-music.edit',            'uses' => 'Backend\BusinessMusicController@edit']);
Route::post('admin/business-music/{id}/update',            ['as' => 'admin.business-music.update',          'uses' => 'Backend\BusinessMusicController@update']);
Route::get('admin/business-music/{id}/delete',             ['as' => 'admin.business-music.delete',          'uses' => 'Backend\BusinessMusicController@destroy']);

Route::get('admin/business-portfolio/list',                ['as' => 'admin.business-portfolio.list',        'uses' => 'Backend\BusinessPortfolioController@index']);
Route::get('admin/business-portfolio/create',              ['as' => 'admin.business-portfolio.create',      'uses' => 'Backend\BusinessPortfolioController@create']);
Route::post('admin/business-portfolio/store',              ['as' => 'admin.business-portfolio.store',       'uses' => 'Backend\BusinessPortfolioController@store']);
Route::get('admin/business-portfolio/{id}/edit',           ['as' => 'admin.business-portfolio.edit',        'uses' => 'Backend\BusinessPortfolioController@edit']);
Route::post('admin/business-portfolio/{id}/update',        ['as' => 'admin.business-portfolio.update',      'uses' => 'Backend\BusinessPortfolioController@update']);
Route::get('admin/business-portfolio/{id}/delete',         ['as' => 'admin.business-portfolio.delete',      'uses' => 'Backend\BusinessPortfolioController@destroy']);

Route::get('admin/business-offer/list',                    ['as' => 'admin.business-offer.list',            'uses' => 'Backend\BusinessOfferController@index']);
Route::get('admin/business-offer/create',                  ['as' => 'admin.business-offer.create',          'uses' => 'Backend\BusinessOfferController@create']);
Route::post('admin/business-offer/store',                  ['as' => 'admin.business-offer.store',           'uses' => 'Backend\BusinessOfferController@store']);
Route::get('admin/business-offer/{id}/edit',               ['as' => 'admin.business-offer.edit',            'uses' => 'Backend\BusinessOfferController@edit']);
Route::post('admin/business-offer/{id}/update',            ['as' => 'admin.business-offer.update',          'uses' => 'Backend\BusinessOfferController@update']);
Route::get('admin/business-offer/{id}/delete',             ['as' => 'admin.business-offer.delete',          'uses' => 'Backend\BusinessOfferController@destroy']);


Route::get('admin/post-manager/list',             ['as' => 'admin.post-manager.list',      'uses' => 'Backend\PostManagerController@index']);
Route::get('admin/post-manager/create',           ['as' => 'admin.post-manager.create',    'uses' => 'Backend\PostManagerController@create']);
Route::get('admin/post-manager/store',            ['as' => 'admin.post-manager.store',     'uses' => 'Backend\PostManagerController@store']);
Route::get('admin/post-manager/{id}/edit',        ['as' => 'admin.post-manager.edit',      'uses' => 'Backend\PostManagerController@edit']);
Route::post('admin/post-manager/{id}/update',     ['as' => 'admin.post-manager.update',    'uses' => 'Backend\PostManagerController@update']);
Route::get('admin/post-manager/{id}/delete',      ['as' => 'admin.post-manager.delete',    'uses' => 'Backend\PostManagerController@destroy']);

Route::get('admin/post-galleries/list',           ['as' => 'admin.post-galleries.list',    'uses' => 'Backend\PostGalleryController@index']);
Route::get('admin/post-galleries/create',         ['as' => 'admin.post-galleries.create',  'uses' => 'Backend\PostGalleryController@create']);
Route::post('admin/post-galleries/store',          ['as' => 'admin.post-galleries.store',  'uses' => 'Backend\PostGalleryController@store']);
Route::get('admin/post-galleries/{id}/edit',      ['as' => 'admin.post-galleries.edit',    'uses' => 'Backend\PostGalleryController@edit']);
Route::post('admin/post-galleries/{id}/update',   ['as' => 'admin.post-galleries.update',  'uses' => 'Backend\PostGalleryController@update']);
Route::get('admin/post-galleries/{id}/delete',    ['as' => 'admin.post-galleries.delete',  'uses' => 'Backend\PostGalleryController@destroy']);


Route::get('admin/event-manager/list',            ['as' => 'admin.event-manager.list',     'uses' => 'Backend\EventManagerController@index']);
Route::get('admin/event-manager/create',          ['as' => 'admin.event-manager.create',   'uses' => 'Backend\EventManagerController@create']);
Route::post('admin/event-manager/store',          ['as' => 'admin.event-manager.store',    'uses' => 'Backend\EventManagerController@store']);
Route::get('admin/event-manager/{id}/edit',       ['as' => 'admin.event-manager.edit',     'uses' => 'Backend\EventManagerController@edit']);
Route::post('admin/event-manager/{id}/update',    ['as' => 'admin.event-manager.update',   'uses' => 'Backend\EventManagerController@update']);
Route::get('admin/event-manager/{id}/delete',     ['as' => 'admin.event-manager.delete',   'uses' => 'Backend\EventManagerController@destroy']);


Route::get('admin/event-galleries/list',          ['as' => 'admin.event-galleries.list',    'uses' => 'Backend\EventGalleryController@index']);
Route::get('admin/event-galleries/create',        ['as' => 'admin.event-galleries.create',  'uses' => 'Backend\EventGalleryController@create']);
Route::post('admin/event-galleries/store',        ['as' => 'admin.event-galleries.store',   'uses' => 'Backend\EventGalleryController@store']);
Route::get('admin/event-galleries/{id}/edit',     ['as' => 'admin.event-galleries.edit',    'uses' => 'Backend\EventGalleryController@edit']);
Route::post('admin/event-galleries/{id}/update',  ['as' => 'admin.event-galleries.update',  'uses' => 'Backend\EventGalleryController@update']);
Route::get('admin/event-galleries/{id}/delete',   ['as' => 'admin.event-galleries.delete',  'uses' => 'Backend\EventGalleryController@destroy']);

});
